module.exports = {
  plugins: [
    'stylelint-order',
  ],
  extends: [
    'stylelint-config-standard'
  ],
  rules: {
    'string-quotes': 'single',
    'at-rule-no-unknown': null,
    'order/order': [
      'declarations',
      {
        'type': 'at-rule',
        'name': 'media',
      },
      {
        'type': 'rule',
        'selector': '^&:\\w'
      },
      {
        'type': 'rule',
        'selector': '^&_'
      },
    ],
    'order/properties-order': [
      [
        'content',
        'position',
        'z-index',
        'top',
        'right',
        'bottom',
        'left',
        'display',
      ],
      {
        unspecified: 'bottom',
      }
    ],
    'selector-pseudo-class-no-unknown': [
      true,
      {
        ignorePseudoClasses: [
          'global',
          'local'
        ]
      }
    ]
  }
}
