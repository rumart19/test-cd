import { connectRouter } from 'connected-react-router';
import { all } from 'redux-saga/effects';

import commonReducer from './common/slice';

export const createRootReducer = history => ({
  router: connectRouter(history),
  common: commonReducer,
});

export const rootSaga = function*() {
  yield all([]);
};
