/* eslint-disable no-param-reassign */

import { createSlice } from 'redux-starter-kit';

import actionTypes from 'utils/actionTypes';
import { MOBILE } from 'constants';

const commonSlice = createSlice({
  name: 'common',
  initialState: {
    isContentLoaded: false,
    isPreloaderHidden: false,
    isHeaderBlend: false,
    viewport: MOBILE,
    pageScrolls: {},
    device: 'desktop',
    isPopStateAction: false,
    isMainVideoShow: false,
    isOpenMenu: false,
    styleColor: '#f9f8e4',
    screenSize: {
      width: 1920,
      height: 1000,
    },
    headerHeight: 0,
  },
  reducers: {
    setIsPopStateAction: (state, { payload }) => {
      state.isPopStateAction = payload;
    },

    setIsOpenMenu: (state, { payload }) => {
      state.isOpenMenu = payload;
    },

    setStyleColor: (state, { payload }) => {
      state.styleColor = payload;
    },

    resetStyleColor: state => {
      state.styleColor = '#f9f8e4';
    },

    setIsHeaderBlend: (state, { payload }) => {
      state.isHeaderBlend = payload;
    },

    contentLoaded: state => {
      state.isContentLoaded = true;
    },

    preloaderHidden: state => {
      state.isPreloaderHidden = true;
    },

    switchViewport: (state, { payload }) => {
      state.viewport = payload;
    },

    changeScreenSize: (state, { payload }) => {
      state.screenSize = payload;
    },

    setPageScroll: (state, { payload }) => {
      state.pageScrolls[payload.page] = payload.position;
    },

    setDevice: (state, { payload }) => {
      state.device = payload;
    },

    clearPageScroll: state => {
      state.pageScrolls = {};
    },

    setHeaderHeight: (state, { payload }) => {
      return {
        ...state,
        headerHeight: payload,
      };
    },

    setShowMainVideo: (state, { payload }) => {
      return {
        ...state,
        isMainVideoShow: payload,
      };
    },
  },
});

export const actions = actionTypes(commonSlice.actions);

export default commonSlice.reducer;
