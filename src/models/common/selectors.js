import { createSelector } from 'reselect';
import { DESKTOP, MOBILE, TABLET } from 'constants';

export const commonSelector = createSelector(
  state => state,
  state => state.common
);

export const contentLoadedSelector = createSelector(
  commonSelector,
  ({ isContentLoaded }) => isContentLoaded
);

export const screenSizeSelector = createSelector(
  commonSelector,
  ({ screenSize }) => screenSize
);

export const preloaderSelector = createSelector(
  commonSelector,
  ({ isPreloaderHidden }) => isPreloaderHidden
);

export const isPopStateSelector = createSelector(
  commonSelector,
  ({ isPopStateAction }) => isPopStateAction
);

export const isOpenMenuSelector = createSelector(
  commonSelector,
  ({ isOpenMenu }) => isOpenMenu
);

export const styleColorSelector = createSelector(
  commonSelector,
  ({ styleColor }) => styleColor
);

export const isHeaderBlendSelector = createSelector(
  commonSelector,
  ({ isHeaderBlend }) => isHeaderBlend
);

export const pageScrollsSelector = createSelector(
  commonSelector,
  common => common.pageScrolls
);

export const viewportSelector = createSelector(
  commonSelector,
  ({ viewport }) => viewport
);

export const isTabletSelector = createSelector(
  viewportSelector,
  viewport => viewport === TABLET
);

export const isMobileSelector = createSelector(
  viewportSelector,
  viewport => viewport === MOBILE
);

export const isDesktopSelector = createSelector(
  viewportSelector,
  viewport => viewport === DESKTOP
);

export const isDeviceDesktopSelector = createSelector(
  commonSelector,
  ({ device }) => device === 'desktop'
);

export const routerSelector = createSelector(
  state => state,
  ({ router }) => router.location.pathname
);

export const isMainVideoShowSelector = createSelector(
  commonSelector,
  ({ isMainVideoShow }) => isMainVideoShow
);

export const headerHeightSelector = createSelector(
  commonSelector,
  ({ headerHeight }) => headerHeight
);
