import useSelector from './useSelector';

import {
  isMobileSelector,
  isTabletSelector,
  isDesktopSelector,
  viewportSelector,
  isDeviceDesktopSelector,
} from 'models/common/selectors';

export default () => ({
  isMobile: useSelector(isMobileSelector),
  isTablet: useSelector(isTabletSelector),
  isDesktop: useSelector(isDesktopSelector),
  viewport: useSelector(viewportSelector),
  isDeviceDesktop: useSelector(isDeviceDesktopSelector),
});
