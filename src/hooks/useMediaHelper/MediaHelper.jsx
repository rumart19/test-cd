import { useRef } from 'react';
import useComponentDidMount from 'hooks/useComponentDidMount';
import useAction from 'hooks/useAction';
import debounce from 'lodash.debounce';
import bowser from 'bowser';

import { actions } from 'models/common/slice';
import {
  onSetGlobalStyle,
  onCheckViewport,
  defaultSettings,
  onSetVhSize,
} from './utils';

export default ref => {
  const switchViewport = useAction(actions.switchViewport);
  const onSetDevice = useAction(actions.setDevice);
  const onSetScreenSize = useAction(actions.changeScreenSize);
  const prevViewport = useRef(null);

  useComponentDidMount(() => {
    defaultSettings();
    onSetVhSize(ref.current);

    const handleResize = () => {
      const { platform, browser, os } = bowser.parse(
        window.navigator.userAgent
      );

      onSetGlobalStyle(platform.type, os.name, browser.name);

      const isCursorExist = !window.matchMedia(
        '(hover: none) and (pointer: coarse)'
      ).matches;

      onSetDevice(isCursorExist ? 'desktop' : 'mobile');
      onSetVhSize(ref.current);

      const { innerWidth, innerHeight } = window;
      onSetScreenSize({ width: innerWidth, height: innerHeight });

      const currentViewport = onCheckViewport();

      if (prevViewport.current !== currentViewport) {
        switchViewport(currentViewport);
        prevViewport.current = currentViewport;
      }
    };

    const handleOrientation = () => {
      window.scrollTo(0, 0);
      setTimeout(handleResize, 300);
    };

    const debounceResize = debounce(handleResize, 200);
    setTimeout(handleResize, 200);

    window.addEventListener('orientationchange', handleOrientation);
    window.addEventListener('resize', handleResize);
    window.addEventListener('resize', debounceResize);

    return () => {
      window.removeEventListener('orientationchange', handleOrientation);
      window.removeEventListener('resize', handleResize);
      window.removeEventListener('resize', debounceResize);
    };
  });
};
