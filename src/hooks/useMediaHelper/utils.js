import {
  MOBILE_MEDIA_QUERY,
  TABLET_MEDIA_QUERY,
  DESKTOP,
  TABLET,
  MOBILE,
} from 'constants';

export const getViewport = (mediaMobile, mediaTablet) => {
  if (mediaMobile.matches) {
    return MOBILE;
  } else if (mediaTablet.matches) {
    return TABLET;
  }
  return DESKTOP;
};

export const onCheckViewport = () => {
  const mediaQueryMobile = window.matchMedia(MOBILE_MEDIA_QUERY);
  const mediaQueryTablet = window.matchMedia(TABLET_MEDIA_QUERY);

  return getViewport(mediaQueryMobile, mediaQueryTablet);
};

export const onSetVhSize = element => {
  window.__height = element
    ? element.getBoundingClientRect().height
    : window.innerHeight;

  const vh = window.__height * 0.01;
  document.documentElement.style.setProperty('--vh', `${vh}px`);
};

export const onSetGlobalStyle = (platform, os, name) => {
  const { classList } = document.body;

  classList.add(`${os}`);
  classList.add(`${name.replace(/\s/g, '')}`);
  if (platform === 'desktop') classList.add('noDevice');
};

export const defaultSettings = () => {
  window.addEventListener('orientationchange', () => {
    setTimeout(() => {
      window.dispatchEvent(new Event('resize'));
    }, 300);
  });

  document.addEventListener('gesturestart', e => {
    e.preventDefault();
  });

  if ('scrollRestoration' in window.history) {
    window.history.scrollRestoration = 'manual';
  }
};
