import slide from 'images/ab--1.jpg';
import slide2 from 'images/ab--2.jpg';
import slide3 from 'images/ab--3.jpg';
import slide4 from 'images/ab--4.jpg';
import slide5 from 'images/ab--5.jpg';

export default [
  {
    id: 1,
    photo: slide,
    title: 'Haley Jones - Stanford',
  },
  {
    id: 2,
    photo: slide2,
    title: 'GRLSWIRL - NYC',
  },
  {
    id: 3,
    photo: slide3,
    title: 'Maya Brady - UCLA',
  },
  {
    id: 4,
    photo: slide4,
    title: 'Chantel Navarro - Los Angeles',
  },
  {
    id: 5,
    photo: slide5,
    title: 'Layla Stubbs - Chicago',
  },
];
