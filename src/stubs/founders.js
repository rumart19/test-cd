import slide from 'images/slide.jpeg';
import slide2 from 'images/slide-2.jpg';
import slide3 from 'images/slide-3.jpg';
import slide4 from 'images/slide-4.jpg';

export default [
  {
    id: 1,
    name: `alex morgan<br /> co—founder`,
    text: `One of the greatest soccer players of all time, Alex has won two World Cups and Olympic gold. She’s an advocate against gender discrimination and recently returned to the pitch after giving birth to her first child, a daughter.`,
    awards: ['Soccer', 'OLYMPIC GOLD MEDALIST'],
    photo: slide,
  },
  {
    id: 2,
    name: `chloe kim<br /> co—founder`,
    text: `Chloe is the youngest woman to win a gold medal in snowboarding at
		just the age of 17. She is the current World, Olympic, Youth Olympic
		and X Games champion in the halfpipe, and the first to win the title
		at all four major events. She was named one of TIME’s most influential
		people.`,
    awards: ['SNOWBOARDER', 'OLYMPIC GOLD MEDALIST'],
    photo: slide2,
  },
  {
    id: 3,
    name: `Simone manuel<br /> co—founder`,
    text: `While setting an Olympic record in the 100m freestyle, Simone was the first African-American woman to win a medal of any type in an individual swimming event.`,
    awards: ['swimmer', '4 olympic medals'],
    photo: slide3,
  },
  {
    id: 4,
    name: `sue bird<br /> co—founder`,
    text: `One of the greatest point guards in basketball history, Sue has won 4 WNBA Championships, 4 Olympic gold medals, 2 NCAA Championships, and 4 FIBA World Cups. She is one of only 11 women to obtain all four accolades.`,
    awards: ['Basketball', '4 olympic medals'],
    photo: slide4,
  },
];
