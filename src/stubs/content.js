import content from 'assets/images/content.jpg';
import content2 from 'assets/images/content2.jpg';
import content3 from 'assets/images/content3.jpg';
import content4 from 'assets/images/content4.jpg';
import content5 from 'assets/images/content5.jpeg';

import bg1 from 'assets/images/bg-1.jpeg';
import bg2 from 'assets/images/bg-2.jpg';
import bg3 from 'assets/images/bg-3.jpg';
import bg4 from 'assets/images/bg-4.jpg';

export default [
  {
    id: 'c1',
    title: 'I AM: JALAIAH - SERIES TRAILER',
    image: content,
    color: '#fc9e54',
    bg: 'rgb(252, 230, 84)',
  },
  {
    id: 'c2',
    title: 'I AM: JALAIAH - SERIES TRAILER',
    image: content2,
    color: '#ff7a00',
    bg: 'yellow',
    imageBg: bg1,
  },
  {
    id: 'c3',
    title: 'I AM: JALAIAH - SERIES TRAILER',
    image: content3,
    color: '#58a393',
    bg: '#000',
    imageBg: bg3,
  },
  {
    id: 'c4',
    title: 'I AM: JALAIAH - SERIES TRAILER',
    image: content4,
    color: '#fc9e54',
    bg: '#000',
    imageBg: bg2,
  },
  {
    id: 'c5',
    title: 'I AM: JALAIAH - SERIES TRAILER',
    image: content5,
    color: '#32cd32',
    bg: '#000',
    imageBg: bg4,
  },
  {
    id: 'c6',
    title: 'I AM: JALAIAH - SERIES TRAILER',
    image: content3,
    color: '#fc9e54',
    bg: 'rgb(88, 169, 255)',
  },
];
