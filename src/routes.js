import Home from 'pages/Home';
import Content from 'pages/Content';

export default [
  {
    path: '/',
    exact: true,
    cache: false,
    component: Home,
    sagasToRun: [],
    title: 'Home',
    sitemap: true,
    settings: {
      idScroll: 'Home',
      withContainer: true,
    },
  },
  {
    path: '/content',
    exact: true,
    cache: false,
    component: Content,
    sagasToRun: [],
    title: 'Content',
    sitemap: true,
    settings: {
      idScroll: 'Content',
      withContainer: true,
    },
  },
];
