import React, { memo } from 'react';

import { PageVisibleHoc } from 'components/PageWrapper';

import ContentPage from 'components/ContentPage';

import s from './Content.scss';

import { bool, string } from 'prop-types';

const Content = ({ isPageIn, idScroll }) => (
  <div className={s.root}>
    <ContentPage idScroll={idScroll} isPageIn={isPageIn} />
  </div>
);

Content.propTypes = {
  isPageIn: bool,
  idScroll: string,
};

export default PageVisibleHoc(memo(Content));
