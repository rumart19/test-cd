import React, { memo } from 'react';

import Hero from 'components/Hero';
import Feature from 'components/Feature';
import Content from 'components/Content';
import Products from 'components/Products';
import Founders from 'components/Founders';
import AboutUs from 'components/AboutUs';
import InnerCircle from 'components/InnerCircle';

import Footer from 'components/Footer';

import useSelector from 'hooks/useSelector';
import { isOpenMenuSelector } from 'models/common/selectors';

import { PageVisibleHoc } from 'components/PageWrapper';

import { ChildScroll, CustomScroll } from 'components/CustomScroll';
import { bool, string } from 'prop-types';

const Home = ({ isPageIn, isAnimateIn, idScroll }) => {
  const isOpenMenu = useSelector(isOpenMenuSelector);

  return (
    <CustomScroll isDisable={!isPageIn || isOpenMenu} idScroll={idScroll}>
      <ChildScroll as="section" hash="Hero" idListener={idScroll}>
        <Hero isPageVisible={isAnimateIn} />
      </ChildScroll>
      <ChildScroll as="section" hash="featured" idListener={idScroll}>
        <Feature idListener={idScroll} />
      </ChildScroll>
      <ChildScroll as="section" hash="content" idListener={idScroll}>
        <Content />
      </ChildScroll>
      <ChildScroll as="section" hash="products" idListener={idScroll}>
        <Products />
      </ChildScroll>
      <ChildScroll as="section" hash="founders" idListener={idScroll}>
        <Founders />
      </ChildScroll>
      <ChildScroll as="section" hash="about" idListener={idScroll}>
        <AboutUs idScroll={idScroll} />
      </ChildScroll>
      <ChildScroll as="section" hash="Circle" idListener={idScroll}>
        <InnerCircle />
      </ChildScroll>
      <ChildScroll as="section" hash="Footer" idListener={idScroll}>
        <Footer />
      </ChildScroll>
    </CustomScroll>
  );
};

Home.propTypes = {
  isPageIn: bool,
  isAnimateIn: bool,
  idScroll: string,
};

export default PageVisibleHoc(memo(Home));
