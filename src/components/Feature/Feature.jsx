import React, { useEffect, useRef } from 'react';
import { string } from 'prop-types';

import Watch from './Watch';
import TickerTop from 'components/Ticker/TickerTop';
import TickerSide from 'components/Ticker/TickerSide';
import TickerBottom from 'components/Ticker/TickerBottom';

import useAction from 'hooks/useAction';
import { actions } from 'models/common/slice';

import useSelector from 'hooks/useSelector';
import { headerHeightSelector } from 'models/common/selectors';

import { useElement } from 'components/CustomScroll';

import s from './Feature.scss';

const Feature = ({ idListener }) => {
  const refFeature = useRef(null);

  const headerHeight = useSelector(headerHeightSelector);
  const [element] = useElement(refFeature, idListener, headerHeight);

  const { isTop } = element;

  const setIsHeaderBlend = useAction(actions.setIsHeaderBlend);

  useEffect(() => {
    setIsHeaderBlend(isTop);
  }, [isTop, setIsHeaderBlend]);

  return (
    <div className={s.root} ref={refFeature}>
      <TickerTop repeat={5} />
      <TickerSide repeat={4} />
      <TickerSide repeat={4} type="right" />
      <TickerBottom repeat={10} />
      <Watch />
    </div>
  );
};

Feature.propTypes = {
  idListener: string,
};

export default Feature;
