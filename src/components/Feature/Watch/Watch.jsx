import React from 'react';

import Svg from 'components/Svg';

import feature from 'assets/images/feature.jpg';

import useAction from 'hooks/useAction';
import { actions as uiActions } from 'models/common/slice';

import { WrapCursor } from 'components/WrapCursor';

import s from './Watch.scss';

const Watch = () => {
  const setIsMainVideoShow = useAction(uiActions.setShowMainVideo);

  const handleClick = () => {
    setIsMainVideoShow(true);
  };

  return (
    <WrapCursor as="div" className={s.root}>
      <div className={s.content} onClick={handleClick}>
        <div className={s.image}>
          <img src={feature} alt="" />
        </div>
        <div className={s.play}>
          <Svg name="play" width="14px" height="14px" />
        </div>
        <div className={s.labels}>
          <a href="/" className={s.label}>
            <span>@TOGETHXR</span>
          </a>
          <div className={s.label}>
            <span>LOS ANGELES — CA</span>
          </div>
        </div>
      </div>
    </WrapCursor>
  );
};

export default Watch;
