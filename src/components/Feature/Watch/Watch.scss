@use '~styles/variables' as *;
@use '~styles/mixins' as *;
@use '~styles/media' as *;
@use '~styles/text' as *;

.root {
  position: relative;
  height: 100%;
  width: 100%;
  padding-top: vwsize(94);
  padding-bottom: vwsize(114);
  padding-left: vwsize(112);
  padding-right: vwsize(112);

  .content {
    border-radius: vwsize(20);
    overflow: hidden;
    cursor: pointer;

    @include hover {
      .play {
        &::after {
          opacity: 1;
        }

        svg {
          fill: $color-light-2;
          transform: scale(1.4);
        }
      }
    }
  }

  .content,
  .image {
    position: relative;
    height: 100%;
    width: 100%;
  }

  .image {
    z-index: 2;

    img {
      position: absolute;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
      object-fit: cover;
    }
  }

  .play {
    position: absolute;
    z-index: 3;
    top: vwsize(50);
    right: vwsize(30);
    display: flex;
    align-items: center;
    justify-content: center;
    width: vwsize(46);
    height: vwsize(46);
    border-radius: 50%;
    background-color: $color-light-2;

    &::after {
      content: '';
      position: absolute;
      z-index: 3;
      top: 50%;
      left: 50%;
      width: vwsize(70);
      height: vwsize(70);
      border-radius: 50%;
      transform: translateX(-50%) translateY(-50%);
      background-color: $color-dark;
      opacity: 0;
      transition: opacity 0.2s linear;
    }

    svg {
      position: relative;
      z-index: 4;
      right: vwsize(-2);
      width: vwsize(14);
      height: vwsize(14);
      transition: fill 0.2s linear;
    }
  }

  .labels {
    position: absolute;
    z-index: 5;
    bottom: vwsize(50);
    left: vwsize(30);
    display: flex;
    flex-direction: column;
    align-items: flex-start;
  }

  .label {
    display: flex;
    align-items: center;
    justify-content: center;
    height: vwsize(46);
    padding-left: vwsize(20);
    padding-right: vwsize(20);
    border-radius: vwsize(23);
    background-color: $color-light-2;
    font-size: vwsize(30);
    line-height: vwsize(-0.48);
    text-transform: uppercase;
    transition: background 0.05s linear, color 0.05s linear;

    &:not(div) {
      @include hover {
        background-color: $color-dark;
        color: $color-white;
      }
    }

    &:not(:last-child) {
      margin-bottom: vwsize(15);
    }
  }
}
