import React from 'react';

import s from './Footer.scss';
import { WrapCursor } from 'components/WrapCursor';
import { func, string } from 'prop-types';

const Footer = ({ currentType, onChangeType }) => {
  const handleChangeType = () => {
    if (onChangeType) onChangeType();
  };

  return (
    <div className={s.root}>
      <WrapCursor as="button" className={s.btn} onClick={handleChangeType}>
        {currentType}
      </WrapCursor>
      <div className={s.num}>1</div>
      <div className={s.num}>2</div>
      <WrapCursor
        as="a"
        href="https://www.youtube.com/channel/UCsKsEa8CDh20DHutrmr6mCg"
        target="_blank"
        rel="noreferrer"
        className={s.btn}
      >
        View more
      </WrapCursor>
    </div>
  );
};

Footer.propTypes = {
  currentType: string.isRequired,
  onChangeType: func,
};

export default Footer;
