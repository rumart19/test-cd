import { array, func } from 'prop-types';
import React, { useRef } from 'react';

import useComponentDidMount from 'hooks/useComponentDidMount';

import gsap from 'gsap';
import draggble from 'utils/draggble';

import useArrayRef from 'hooks/useArrayRef';

import Card from './Card';

import s from './Full.scss';

const Full = ({ list, onEnter, onLeave }) => {
  const [refs, setRefs] = useArrayRef();
  const sliderRef = useRef(null);

  let draggable;

  const animateIn = index => {
    const animRefs = refs.current.filter((_, i) => {
      return i !== index;
    });

    gsap.to(animRefs, {
      opacity: 0,
      delay: 0.3,
    });
  };

  const animateOut = index => {
    const animRefs = refs.current.filter((_, i) => {
      return i !== index;
    });

    gsap.to(animRefs, {
      opacity: 1,
      delay: 0.3,
    });
  };

  const handleLeaveAnim = index => {
    animateOut(index);
    if (onLeave) onLeave(index);
  };

  const handleEnterAnim = index => {
    animateIn(index);
    if (onEnter) onEnter(index);
  };

  useComponentDidMount(() => {
    draggable = draggble(sliderRef.current, refs, {
      autoPlay: false,
    });

    draggable.init();

    return () => {
      if (draggable) draggable.destroy();
    };
  });

  return (
    <div className={s.root} ref={sliderRef}>
      {list.map((item, index) => (
        <div
          className={s.item}
          key={item.id}
          ref={setRefs}
          onMouseEnter={() => handleEnterAnim(index)}
          onMouseLeave={() => handleLeaveAnim(index)}
        >
          <Card data={item} />
        </div>
      ))}
    </div>
  );
};

Full.propTypes = {
  list: array.isRequired,
  onEnter: func,
  onLeave: func,
};

export default Full;
