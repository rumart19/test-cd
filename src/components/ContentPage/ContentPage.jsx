import React, { useRef, useState, memo } from 'react';

import Grid from './Grid';

import content from 'stubs/content';

import { bool, func, string } from 'prop-types';
import { gsap } from 'gsap';

import Footer from './Footer';
import Full from './Full';

import {
  ChildScroll,
  CustomScrollHoc,
  CustomScroll,
} from 'components/CustomScroll';

import useAction from 'hooks/useAction';
import { actions as uiActions } from 'models/common/slice';

import { STYLE_COLOR } from 'constants';

import useComponentDidMount from 'hooks/useComponentDidMount';

import s from './ContentPage.scss';

const ContentPage = ({ idScroll, onScrollTo, isPageIn }) => {
  const rootRef = useRef(null);
  const bgRef = useRef(null);
  const setColor = useAction(uiActions.setStyleColor);

  const [type, setType] = useState('grid');

  const curElement = useRef(null);

  const [prevIndex, setPrevIndex] = useState(0);

  const intialContent = {
    bg: '#000',
    imageBg: null,
    color: STYLE_COLOR,
  };

  const animInit = () => {
    gsap.set(curElement.current, {
      opacity: 1,
      backgroundColor: intialContent.bg,
    });
  };

  const addBg = (currentContent, prevContent) => {
    const prev = curElement.current;
    const newEl = document.createElement('div');
    bgRef.current.appendChild(newEl);

    const tl = gsap.timeline({ paused: true, delay: 0.3 });

    tl.set(newEl, {
      backgroundImage: currentContent.imageBg
        ? `url(${currentContent.imageBg})`
        : 'none',
    })
      .fromTo(
        newEl,
        {
          opacity: 0,
        },
        {
          opacity: 1,
          backgroundColor: currentContent.bg,
        }
      )
      .set(prev, {
        backgroundColor: prevContent.bg,
        backgroundImage: prevContent.imageBg
          ? `url(${prevContent.imageBg})`
          : 'none',
      })
      .to(
        prev,
        {
          onStart: () => {
            setColor(currentContent.color);
          },
          onComplete: () => {
            prev.remove();
          },
        },
        0
      )
      .play();

    curElement.current = newEl;
  };

  useComponentDidMount(() => {
    animInit();
  });

  const handleEnter = index => {
    addBg(content[index], content[prevIndex]);
    setPrevIndex(index);
  };

  const handleLeave = () => {
    addBg(intialContent, content[prevIndex]);
    setPrevIndex(0);
  };

  const renderContent = t => {
    switch (t) {
      case 'grid':
        return (
          <Grid list={content} onEnter={handleEnter} onLeave={handleLeave} />
        );
      case 'full':
        return (
          <Full list={content} onEnter={handleEnter} onLeave={handleLeave} />
        );
      default:
        return 'none';
    }
  };

  const handleChangeType = () => {
    onScrollTo('Content', 0);
    setType(type === 'grid' ? 'full' : 'grid');
  };

  return (
    <div ref={rootRef} className={s.root}>
      <div className={s.background} ref={bgRef}>
        <div ref={curElement} />{' '}
      </div>
      <CustomScroll
        isDisable={!isPageIn}
        idScroll={idScroll}
        className={s.scroll}
      >
        <ChildScroll
          as="section"
          hash="Content"
          idListener={idScroll}
          className={s.content}
        >
          {renderContent(type)}
        </ChildScroll>
      </CustomScroll>
      <Footer
        currentType={type === 'grid' ? 'full' : 'grid'}
        onChangeType={handleChangeType}
      />
    </div>
  );
};

ContentPage.propTypes = {
  idScroll: string,
  onScrollTo: func,
  isPageIn: bool,
};

export default CustomScrollHoc(memo(ContentPage));
