import React from 'react';

import s from './Card.scss';

import useAction from 'hooks/useAction';
import { actions as uiActions } from 'models/common/slice';

import { WrapCursor } from 'components/WrapCursor';
import { object } from 'prop-types';

const Card = ({ data }) => {
  const setIsMainVideoShow = useAction(uiActions.setShowMainVideo);

  const handleClick = () => {
    setIsMainVideoShow(true);
  };

  return (
    <div className={s.root} onClick={handleClick}>
      <WrapCursor as="div" className={s.wrapper}>
        <div className={s.title}>{data.title}</div>
        <div className={s.image}>
          <img src={data.image} alt="" />
        </div>
      </WrapCursor>
    </div>
  );
};

Card.propTypes = {
  data: object.isRequired,
};

export default Card;
