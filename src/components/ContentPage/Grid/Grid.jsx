import { array, func } from 'prop-types';
import React from 'react';

import { gsap } from 'gsap';

import Card from './Card';

import useArrayRef from 'hooks/useArrayRef';

import s from './Grid.scss';

const Grid = ({ list, onEnter, onLeave }) => {
  const [refs, setRefs] = useArrayRef();

  const animateIn = index => {
    const animRefs = refs.current.filter((_, i) => {
      return i !== index;
    });

    gsap.to(animRefs, {
      opacity: 0,
      delay: 0.3,
    });
  };

  const animateOut = index => {
    const animRefs = refs.current.filter((_, i) => {
      return i !== index;
    });

    gsap.to(animRefs, {
      opacity: 1,
      delay: 0.3,
    });
  };

  const handleLeave = index => {
    animateOut(index);
    if (onLeave) onLeave(index);
  };

  const handleEnter = index => {
    animateIn(index);
    if (onEnter) onEnter(index);
  };

  return (
    <div className={s.root}>
      <div className={s.inner}>
        {list.map((item, index) => (
          <div
            className={s.item}
            ref={setRefs}
            key={item.id}
            onMouseEnter={() => handleEnter(index)}
            onMouseLeave={() => handleLeave(index)}
          >
            <Card data={item} />
          </div>
        ))}
      </div>
    </div>
  );
};

Grid.propTypes = {
  list: array.isRequired,
  onEnter: func,
  onLeave: func,
};

export default Grid;
