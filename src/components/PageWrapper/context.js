import { createContext } from 'react';

export const PageVisible = createContext();
export const PageScrollId = createContext();
