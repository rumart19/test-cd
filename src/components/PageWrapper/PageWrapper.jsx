/* eslint-disable */
import React, { Fragment, createRef, PureComponent } from 'react';
import { string, any, bool } from 'prop-types';
import { Helmet } from 'react-helmet';
import { connect } from 'react-redux';
import { gsap } from 'gsap';

import { Loader } from 'components/Loader';

import config from 'config';

import { PAGE_TRANSITION } from 'constants';

import { cursorLeave } from 'components/CustomCursor/useCursorEvents';

import {
  contentLoadedSelector,
  preloaderSelector,
  isOpenMenuSelector,
} from 'models/common/selectors';

import { PageVisible, PageScrollId } from './context';

import s from './PageWrapper.scss';

@connect(state => ({
  isLoadedContent: contentLoadedSelector(state),
  isPreloaderHidden: preloaderSelector(state),
  isOpenMenu: isOpenMenuSelector(state),
}))

class PageWrapper extends PureComponent {
  constructor(props) {
    super(props);
    const { isLoadedContent, idScroll } = props;

    this.state = {
      isPageIn: false,
      isAnimateIn: false,
      isLoadedContent,
    };

    this.isMountloadContent = isLoadedContent;

    this.root = createRef();

    this.dataPageScrollId = { idScroll };

    this.mountWithOpenMenu = props.isOpenMenu;
  }

  componentDidMount() {
    cursorLeave();
  }

  componentDidUpdate(prevProps) {
    const { isLoadedContent, isVisiblePage, isPreloaderHidden } = prevProps;

    const {
      isLoadedContent: nextIsLoadedContent,
      isVisiblePage: nextIsVisiblePage,
      isPreloaderHidden: nextIsPreloaderHidden,
    } = this.props;

    if (!this.isMountloadContent) {
      if (isPreloaderHidden !== nextIsPreloaderHidden && nextIsLoadedContent) {
        this.isMountloadContent = nextIsLoadedContent;

        this.setState({
          isPageIn: true,
          isAnimateIn: true,
          isLoadedContent: nextIsLoadedContent,
        });
      }
      return;
    }

    if (isVisiblePage !== nextIsVisiblePage && isLoadedContent) {
      if (nextIsVisiblePage) {
        this.timerUpdate = gsap.to({}, ((PAGE_TRANSITION * 2) / 1000) * 0.8, {
          onComplete: () => {
            this.setState({ isPageIn: true, isAnimateIn: true });
          },
        });
        return;
      }

      this.setState({ isPageIn: false });
    }
  }

  componentWillUnmount() {}

  render() {
    const { title, children } = this.props;

    const helmetTitle = title || config.app.title;

    return (
      <Fragment>
        <Helmet title={helmetTitle} meta={config.app.meta} />
        <Loader
          className={s.root}
          as="main"
          id="pageWrapper"
          style={{ pointerEvents: this.props.isVisiblePage ? '' : 'none', }}
        >
          <PageScrollId.Provider value={this.dataPageScrollId}>
            <PageVisible.Provider value={this.state}>
              {children}
            </PageVisible.Provider>
          </PageScrollId.Provider>
        </Loader>
      </Fragment>
    );
  }
}

PageWrapper.propTypes = {
  title: string,
  children: any,
  isDesktop: bool,
  isLoadedContent: bool,
  isVisiblePage: bool,
  className: string,
  colorScheme: string,
  color: string,
  withContainer: bool,
};

export default PageWrapper;
