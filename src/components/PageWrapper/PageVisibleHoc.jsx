import React from 'react';
import { PageVisible } from './context';

const PageVisibleHoc = WrappedComponent => {
  const HOC = props => (
    <PageVisible.Consumer>
      {context => <WrappedComponent {...props} {...context} />}
    </PageVisible.Consumer>
  );

  const getDisplayName = WrappedComponentIn =>
    WrappedComponentIn.displayName || WrappedComponentIn.name || 'Component';

  HOC.displayName = `PageVisibleHoc(${getDisplayName(WrappedComponent)})`;

  return HOC;
};

export default PageVisibleHoc;
