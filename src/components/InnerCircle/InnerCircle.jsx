import React from 'react';

import bg from 'images/inner-circle.jpeg';

import { WrapCursor } from 'components/WrapCursor';
import Svg from 'components/Svg';

import s from './InnerCircle.scss';

const InnerCircle = () => {
  return (
    <div className={s.root}>
      <div className={s.background}>
        <img src={bg} alt="" />
      </div>
      <div className={s.inner}>
        <div className={s.header}>
          <div>join us</div>
          <div>
            togethxr <span>with</span> you
          </div>
          <div>inner circle</div>
        </div>
        <div className={s.content}>
          <WrapCursor as="a" href="/" className={s.link}>
            <Svg
              name="arrow"
              color="none"
              stroke="light"
              width="25px"
              height="23px"
            />
            <Svg
              name="arrow"
              color="none"
              stroke="light"
              width="25px"
              height="23px"
            />
            <Svg
              name="arrow"
              color="none"
              stroke="light"
              width="25px"
              height="23px"
            />
            <Svg
              name="arrow"
              color="none"
              stroke="light"
              width="25px"
              height="23px"
            />
            <span>Join our inner circle</span>
          </WrapCursor>
        </div>
        <div className={s.footer}>
          <div className={s.text}>
            Joining our inner circle means you help us grow our community of
            like-minded women across the U.S. and around the world. Most of all,
            you represent TOGETHXR’s values and bring your true self to the
            world every day.
          </div>
        </div>
      </div>
    </div>
  );
};

export default InnerCircle;
