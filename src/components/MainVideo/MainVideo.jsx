import { func } from 'prop-types';
import React, { useState } from 'react';

import { WrapCursor } from 'components/WrapCursor';

import { cursorLeave } from 'components/CustomCursor/useCursorEvents';

import useComponentDidMount from 'hooks/useComponentDidMount';

import s from './MainVideo.scss';

const MainVideo = ({ onClose }) => {
  const [isClosing, setIsclosing] = useState(false);

  const handleClose = () => {
    setIsclosing(true);
    if (onClose) onClose();
    cursorLeave();
  };

  useComponentDidMount(() => {
    return () => {};
  });

  return (
    <div className={s.root}>
      <WrapCursor
        as="div"
        cursorText="close"
        onClick={handleClose}
        disableCursor={isClosing}
        className={s.bg}
      />
      <WrapCursor
        as="div"
        disableCursor={isClosing}
        isHidden
        className={s.wrapper}
      >
        <iframe
          title="main video"
          src="https://www.youtube.com/embed/6neFzSETx7E?autoplay=1&amp;enablejsapi=1&amp;controls=1&amp;showinfo=0&amp;rel=0&amp;listType=playlist"
          frameBorder="0"
          allow="autoplay; encrypted-media"
        />
      </WrapCursor>
    </div>
  );
};

MainVideo.propTypes = {
  onClose: func,
};

MainVideo.defaultProps = {
  onClose: null,
};

export default MainVideo;
