import React from 'react';

import classes from 'classnames';

import s from './Item.scss';
import { string } from 'prop-types';

const Item = React.forwardRef(({ className }, ref) => {
  return (
    <div className={classes(s.root, className)} ref={ref}>
      <span className={s.title}>
        * FOUNDED BY — ALEX MORGAN, CHLOE KIM, SIMONE MANUEL, SUE BIRD
      </span>
    </div>
  );
});

Item.propTypes = {
  className: string,
};

Item.defaultProps = {
  className: '',
};

export default Item;
