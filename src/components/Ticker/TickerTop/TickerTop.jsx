import React, { useRef } from 'react';
import { number } from 'prop-types';

import { gsap } from 'gsap';
import useComponentDidMount from 'hooks/useComponentDidMount';

import useArrayRef from 'hooks/useArrayRef';

import Item from './Item';

import s from './TickerTop.scss';

const TickerTop = ({ repeat }) => {
  const items = Array(repeat).fill('');

  const [refs, setRefs] = useArrayRef();
  const tween = useRef(null);

  const initAnimation = () => {
    if (tween.current) {
      tween.current.pause().kill();
    }

    const elements = refs.current;

    if (elements.length > 0) {
      const itemWidth = elements[0].clientWidth;
      const distantion = itemWidth * repeat;

      gsap.set(elements, {
        x: i => i * itemWidth,
      });

      tween.current = gsap.to(elements, {
        duration: 30,
        ease: 'none',
        x: `+=${distantion}`,
        modifiers: {
          x: gsap.utils.unitize(x => {
            return (parseFloat(x) % distantion) - itemWidth;
          }),
        },
        repeat: -1,
      });
    }
  };

  useComponentDidMount(() => {
    initAnimation();

    window.addEventListener('resize', initAnimation);

    return () => {
      window.removeEventListener('resize', initAnimation);
      if (tween.current) tween.current.pause().kill();
    };
  });

  return (
    <div className={s.root}>
      <div className={s.inner}>
        {items.map((_, i) => (
          <Item
            className={s.item}
            ref={setRefs}
            // eslint-disable-next-line
            key={`tick-${i}`}
          />
        ))}
      </div>
    </div>
  );
};

TickerTop.propTypes = {
  repeat: number,
};

TickerTop.defaultProps = {
  repeat: 5,
};

export default TickerTop;
