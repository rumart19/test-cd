import React from 'react';

import classes from 'classnames';

import Svg from 'components/Svg';

import s from './Item.scss';
import { string } from 'prop-types';

const Item = React.forwardRef(({ className }, ref) => {
  return (
    <div className={classes(s.root, className)} ref={ref}>
      <span className={s.title}>LATEST FEATURE</span>
      <span className={s.small}>INTRODUCING</span>
      <span className={s.symbols}>
        <Svg
          name="star"
          color="dark"
          stroke="none"
          width="32px"
          height="29px"
        />
        <span className={s.small}>TOGETHXR</span>
      </span>
    </div>
  );
});

Item.propTypes = {
  className: string,
};

Item.defaultProps = {
  className: '',
};

export default Item;
