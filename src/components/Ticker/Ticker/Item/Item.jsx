import React from 'react';

import classes from 'classnames';

import Svg from 'components/Svg';

import s from './Item.scss';
import { string } from 'prop-types';

const Item = React.forwardRef(({ className }, ref) => {
  return (
    <div className={classes(s.root, className)} ref={ref}>
      <span className={s.title}>SHOP TOGETHXR</span>
      <span className={s.time}>02.21</span>
      <span className={s.drop}>
        <span>NEW DR</span>
        <Svg name="globe" width="22px" height="22px" />
        <span>P</span>
      </span>
    </div>
  );
});

Item.propTypes = {
  className: string,
};

Item.defaultProps = {
  className: '',
};

export default Item;
