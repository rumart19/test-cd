import React, { useRef } from 'react';
import { number, string } from 'prop-types';

import { gsap } from 'gsap';
import useComponentDidMount from 'hooks/useComponentDidMount';

import useArrayRef from 'hooks/useArrayRef';

import classes from 'classnames';
import modsClasses from 'utils/modsClasses';

import Item from './Item';

import s from './Ticker.scss';

const Ticker = ({ type, repeat, className, duration }) => {
  const items = Array(repeat).fill('');

  const [refs, setRefs] = useArrayRef();
  const tween = useRef(null);

  const mods = modsClasses(s, { type });

  const initAnimation = () => {
    if (tween.current) {
      tween.current.pause().kill();
    }

    const elements = refs.current;

    if (elements.length > 0) {
      const itemHeight = elements[0].clientHeight;
      const distantion = itemHeight * repeat;

      gsap.set(elements, {
        y: i => i * itemHeight,
      });

      tween.current = gsap.to(elements, {
        duration,
        ease: 'none',
        y: `+=${distantion}`,
        modifiers: {
          y: gsap.utils.unitize(y => {
            if (type === 'right') {
              return y < 0
                ? (parseFloat(y) % distantion) + distantion - itemHeight
                : y - itemHeight;
            }
            return (parseFloat(y) % distantion) - itemHeight;
          }),
        },
        repeat: -1,
      });
    }
  };

  useComponentDidMount(() => {
    initAnimation();

    window.addEventListener('resize', initAnimation);

    return () => {
      window.removeEventListener('resize', initAnimation);
      if (tween.current) tween.current.pause().kill();
    };
  });

  return (
    <div className={classes(s.root, mods, className)}>
      <div className={s.inner}>
        {items.map((_, i) => (
          <Item
            className={s.item}
            ref={setRefs}
            // eslint-disable-next-line
            key={`tick-${i}`}
          />
        ))}
      </div>
    </div>
  );
};

Ticker.propTypes = {
  repeat: number,
  type: string,
  className: string,
  duration: number,
};

Ticker.defaultProps = {
  repeat: 5,
  type: '',
};

export default Ticker;
