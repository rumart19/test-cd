import React, { useRef, useState } from 'react';
import { number } from 'prop-types';

import { gsap } from 'gsap';
import useComponentDidMount from 'hooks/useComponentDidMount';

import useArrayRef from 'hooks/useArrayRef';

import useAction from 'hooks/useAction';
import { actions as uiActions } from 'models/common/slice';

import Item from './Item';

import s from './TickerBottom.scss';

const TickerBottom = ({ repeat }) => {
  const [isActive, setIsActive] = useState(false);
  const setIsMainVideoShow = useAction(uiActions.setShowMainVideo);

  const items = Array(repeat).fill('');

  const [refs, setRefs] = useArrayRef();
  const tween = useRef(null);

  const initAnimation = () => {
    if (tween.current) {
      tween.current.pause().kill();
    }

    const elements = refs.current;

    if (elements.length > 0) {
      const style =
        elements[0].currentStyle || window.getComputedStyle(elements[0]);

      const gap = parseFloat(style.marginRight);

      const itemWidth = elements[0].clientWidth + gap;
      const distantion = itemWidth * repeat;

      gsap.set(elements, {
        x: i => i * itemWidth,
      });

      tween.current = gsap.to(elements, {
        duration: 30,
        ease: 'none',
        x: `+=${distantion}`,
        modifiers: {
          x: gsap.utils.unitize(x => {
            return (parseFloat(x) % distantion) - itemWidth;
          }),
        },
        repeat: -1,
      });
    }
  };

  const handleClick = () => {
    setIsMainVideoShow(true);
  };

  const handleMouseEnter = () => {
    setIsActive(true);
  };

  const handleMouseLeave = () => {
    setIsActive(false);
  };

  useComponentDidMount(() => {
    initAnimation();

    window.addEventListener('resize', initAnimation);

    return () => {
      window.removeEventListener('resize', initAnimation);
      if (tween.current) tween.current.pause().kill();
    };
  });

  return (
    <div
      className={s.root}
      onMouseEnter={handleMouseEnter}
      onMouseLeave={handleMouseLeave}
    >
      <div className={s.inner}>
        {items.map((_, i) => (
          <Item
            className={s.item}
            ref={setRefs}
            isActive={isActive}
            onClick={handleClick}
            // eslint-disable-next-line
            key={`tick-${i}`}
          />
        ))}
      </div>
    </div>
  );
};

TickerBottom.propTypes = {
  repeat: number,
};

TickerBottom.defaultProps = {
  repeat: 5,
};

export default TickerBottom;
