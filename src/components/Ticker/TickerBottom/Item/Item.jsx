import React from 'react';

import classes from 'classnames';

import Svg from 'components/Svg';

import { WrapCursor } from 'components/WrapCursor';

import s from './Item.scss';
import { bool, func, string } from 'prop-types';

const Item = React.forwardRef(({ className, isActive, onClick }, ref) => {
  const hanldeClick = () => {
    if (onClick) onClick();
  };

  return (
    <WrapCursor
      as="div"
      className={classes(s.root, className, {
        [s.isActive]: isActive,
      })}
      ref={ref}
      onClick={hanldeClick}
    >
      <span className={s.title}>WATCH NOW</span>
      <Svg name="arrow" color="none" stroke="dark" width="25px" height="23px" />
    </WrapCursor>
  );
});

Item.propTypes = {
  className: string,
  isActive: bool,
  onClick: func,
};

Item.defaultProps = {
  className: '',
  isActive: false,
  onClick: null,
};

export default Item;
