import React, { useState } from 'react';
import classes from 'classnames';

import { WrapCursor } from 'components/WrapCursor';

import s from './AudioButton.scss';

const AudioButton = () => {
  const [isActive, setIsActive] = useState(false);

  const handleClick = () => {
    setIsActive(!isActive);
    console.log(isActive);
  };

  return (
    <WrapCursor
      as="button"
      className={classes(s.root, {
        [s.isActive]: isActive,
      })}
      type="button"
      onClick={handleClick}
    >
      <div className={s.item} />
      <div className={s.item} />
      <div className={s.item} />
      <div className={s.item} />
      <div className={s.item} />
    </WrapCursor>
  );
};

export default AudioButton;
