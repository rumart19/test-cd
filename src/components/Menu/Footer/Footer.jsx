import React from 'react';

import Svg from 'components/Svg';

import { WrapCursor } from 'components/WrapCursor';

import s from './Footer.scss';

const Footer = () => {
  return (
    <div className={s.root}>
      <div className={s.item}>
        <div>
          <WrapCursor
            as="a"
            href="mailto:press@togethxr.com"
            className={s.bold}
          >
            press@togethxr.com
          </WrapCursor>
        </div>
        <div>
          <WrapCursor as="a" href="/">
            Shipping & Handling
          </WrapCursor>
        </div>
      </div>
      <div className={s.info}>
        <span className={s.bold}>Togethxr</span>
        <Svg name="copy" width="37" height="26" />
        <span>Copyright 2021 LA / CA</span>
      </div>
      <div className={s.item}>
        <div>
          <div>
            <WrapCursor
              as="a"
              href="mailto:info@togethxr.com"
              className={s.bold}
            >
              info@togethxr.com
            </WrapCursor>
          </div>
          <WrapCursor as="a" href="/">
            Privacy & Information
          </WrapCursor>
        </div>
      </div>
    </div>
  );
};

export default Footer;
