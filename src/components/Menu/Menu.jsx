import React, { useState } from 'react';

import { WrapCursor } from 'components/WrapCursor';
import Footer from './Footer';

import { ChildScroll, CustomScroll } from 'components/CustomScroll';

import classes from 'classnames';

import { MAIN_MENU } from 'constants';

import s from './Menu.scss';

const Menu = () => {
  const [currentItem, setCurrentItem] = useState(null);

  const handleEnter = id => {
    setCurrentItem(id);
  };

  const handleLeave = () => {
    setCurrentItem(null);
  };

  return (
    <div className={s.root}>
      <CustomScroll idScroll="Menu" isPageIn>
        <ChildScroll idListener="Menu">
          <nav className={s.nav}>
            <ul className={s.list}>
              {MAIN_MENU.map(item => (
                <li className={s.item} key={item.id}>
                  <WrapCursor
                    to={item.to}
                    className={classes(s.link, {
                      [s.hover]: item.id !== currentItem && currentItem,
                    })}
                    key={item.id}
                    onEnter={() => handleEnter(item.id)}
                    onLeave={handleLeave}
                  >
                    {item.title}
                  </WrapCursor>
                </li>
              ))}
            </ul>
          </nav>
        </ChildScroll>
      </CustomScroll>
      <Footer />
    </div>
  );
};

export default Menu;
