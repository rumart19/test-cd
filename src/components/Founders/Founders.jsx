import React from 'react';

import classes from 'classnames';

import Plus from 'components/Plus';
import Slider from './Slider';

import s from './Founders.scss';

const Founders = () => {
  return (
    <div className={s.root}>
      <div className={s.header}>
        <div className={s.title}>Founders</div>
      </div>
      <div className={classes(s.pluses, s.plusesTop)}>
        <Plus />
        <Plus />
      </div>
      <Slider className={s.slider} />
      <div className={classes(s.pluses, s.plusesBottom)}>
        <Plus />
        <Plus />
      </div>
    </div>
  );
};

export default Founders;
