import React from 'react';

import classes from 'classnames';

import { WrapPosition } from 'components/WrapCursor';
import Svg from 'components/Svg';

import s from './Item.scss';
import { object, string } from 'prop-types';

const Item = React.forwardRef(({ className, data }, ref) => {
  const getKey = (t, index) => {
    return `${t}-${index}`;
  };

  return (
    <WrapPosition
      as="div"
      cursorText={data.name}
      className={classes(s.root, className)}
      ref={ref}
    >
      <div className={s.image}>
        <img src={data.photo} alt="" />
      </div>
      {data.text && (
        <div className={s.info}>
          <div className={s.text}>{data.text}</div>
          <div className={s.awards}>
            {data.awards.map((aw, i) => (
              <div className={s.award} key={getKey('aw', i)}>
                <Svg name="xicon" width="27px" height="38px" />
                <span>{aw}</span>
              </div>
            ))}
          </div>
        </div>
      )}
    </WrapPosition>
  );
});

Item.propTypes = {
  className: string,
  data: object.isRequired,
};

Item.defaultProps = {
  className: '',
};

export default Item;
