import React, { useRef } from 'react';

import { string } from 'prop-types';

import Item from './Item';

import draggble from 'utils/draggble';

import useComponentDidMount from 'hooks/useComponentDidMount';

import useArrayRef from 'hooks/useArrayRef';

import classes from 'classnames';

import founders from 'stubs/founders';

import s from './Slider.scss';

const Slider = ({ className }) => {
  const [refs, setRefs] = useArrayRef([]);
  const sliderRef = useRef(null);

  let draggable;

  useComponentDidMount(() => {
    draggable = draggble(sliderRef.current, refs, {
      autoPlay: true,
    });

    draggable.init();

    return () => {
      if (draggable) draggable.destroy();
    };
  });

  return (
    <>
      <div className={classes(s.root, className)} ref={sliderRef}>
        {founders.map(item => (
          <div className={s.item} ref={setRefs} key={item.id}>
            <Item data={item} />
          </div>
        ))}
      </div>
    </>
  );
};

Slider.propTypes = {
  className: string,
};

export default Slider;
