import React from 'react';

import Svg from 'components/Svg';

import { WrapCursor } from 'components/WrapCursor';

import s from './Images.scss';
import { string } from 'prop-types';

const Images = ({ url }) => {
  return (
    <div className={s.root}>
      <div className={s.inner}>
        <div className={s.arrows}>
          <div className={s.arrow}>
            <Svg name="triangle" width="13" height="22" />
            <span>2</span>
          </div>
          <div className={s.arrow}>
            <Svg name="triangle" width="13" height="22" />
          </div>
        </div>
        <div className={s.image}>
          <img src={url} alt="" />
        </div>
        <WrapCursor as="button" type="button" className={s.btn}>
          <Svg name="arrowr" width="18px" height="18px" />
          <span>VIEW ALL OUR CONTENT</span>
          <Svg name="arrowr" width="18px" height="18px" />
        </WrapCursor>
      </div>
    </div>
  );
};

Images.propTypes = {
  url: string,
};

export default Images;
