import React, { useEffect, useRef, useState } from 'react';

import Header from './Header';
import Images from './Images';
import content from 'assets/images/content.jpg';
import content2 from 'assets/images/content2.jpg';
import content3 from 'assets/images/content3.jpg';
import content4 from 'assets/images/content4.jpg';
import content5 from 'assets/images/content5.jpeg';

import useComponentDidMount from 'hooks/useComponentDidMount';

const CONTENTS_IMAGES = [content, content2, content3, content4, content5];

import s from './Content.scss';

const Content = () => {
  const [image, setImage] = useState(CONTENTS_IMAGES[0]);
  const [currentIndex, setCurrentIndex] = useState(0);
  const [perPixel, setPerPixel] = useState(0);

  const rootRef = useRef(null);

  let oldCursorY = 0;
  let oldCursorX = 0;
  let totalDistance = 0;

  const handlePerPixelSet = () => {
    const widthPerPixel =
      rootRef.current.clientWidth / (CONTENTS_IMAGES.length * 2);
    setPerPixel(widthPerPixel);
  };

  useComponentDidMount(() => {
    handlePerPixelSet();

    window.addEventListener('resize', handlePerPixelSet);

    return () => {
      window.removeEventListener('resize', handlePerPixelSet);
    };
  });

  const handleMouseMove = e => {
    if (oldCursorX) {
      totalDistance += Math.sqrt(
        (oldCursorY - e.clientY) ** 2 + (oldCursorX - e.clientX) ** 2
      );
    }

    if (currentIndex === CONTENTS_IMAGES.length - 1) {
      setCurrentIndex(0);
    }

    if (totalDistance >= perPixel) {
      totalDistance = 0;
      setCurrentIndex(prev => prev + 1);
    }

    oldCursorX = e.clientX;
    oldCursorY = e.clientY;
  };

  useEffect(() => {
    setImage(CONTENTS_IMAGES[currentIndex]);
  }, [currentIndex]);

  return (
    <div ref={rootRef} className={s.root} onMouseMove={e => handleMouseMove(e)}>
      <Header />
      <Images url={image} />
    </div>
  );
};

export default Content;
