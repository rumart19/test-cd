import React from 'react';

import GsapAnimate from 'components/GsapAnimate';
import Svg from 'components/Svg';
import s from './Hero.scss';

const Hero = () => {
  const titleAnimation = {
    in: 'blurIn',
    out: 'blurOut',
  };

  return (
    <div className={s.root}>
      <div className={s.inner}>
        <div className={s.title}>
          <GsapAnimate
            className={s.btn}
            duration={1500}
            animateOnMount
            animations={titleAnimation}
          >
            <Svg name="thxr" width="1846px" height="196px" />
          </GsapAnimate>
        </div>
      </div>
      <div className={s.pluses}>
        <div className={s.pLine} />
        <div className={s.pLine}>
          <div className={s.plus} />
          <div className={s.plus} />
          <div className={s.plus} />
          <div className={s.plus} />
          <div className={s.plus} />
          <div className={s.plus} />
        </div>
        <div className={s.pLine}>
          <div className={s.plus} />
          <div className={s.plus} />
          <div className={s.plus} />
          <div className={s.plus} />
          <div className={s.plus} />
          <div className={s.plus} />
        </div>
      </div>
      <video
        src="background.mp4"
        className={s.bg}
        autoPlay
        muted
        loop
        width="100%"
        height="100%"
      >
        <track kind="captions" />
      </video>
    </div>
  );
};

export default Hero;
