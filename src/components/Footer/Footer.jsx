import React from 'react';

import Copyright from './Copyright';
import Ticker from './Ticker';

import s from './Footer.scss';

const Footer = () => {
  return (
    <div className={s.root} id="footer">
      <div className={s.top}>
        <Ticker />
      </div>
      <div className={s.bottom}>
        <Copyright />
      </div>
    </div>
  );
};

export default Footer;
