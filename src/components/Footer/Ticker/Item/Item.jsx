import React from 'react';

import classes from 'classnames';

import Svg from 'components/Svg';

import s from './Item.scss';
import { string } from 'prop-types';

const Item = React.forwardRef(({ className }, ref) => {
  return (
    <a
      href="https://www.nytimes.com/2021/03/02/sports/womens-sports-coverage.html"
      target="_blank"
      rel="noreferrer"
      className={classes(s.root, className)}
      ref={ref}
    >
      <span className={s.small}>Check our feature in</span>
      <span className={s.big}>THE NEW YORK TIMES</span>
      <span className={s.symbols}>
        <Svg
          name="star"
          color="dark"
          stroke="none"
          width="32px"
          height="29px"
        />
      </span>
    </a>
  );
});

Item.propTypes = {
  className: string,
};

Item.defaultProps = {
  className: '',
};

export default Item;
