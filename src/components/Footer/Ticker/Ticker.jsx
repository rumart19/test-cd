import React, { useRef } from 'react';
import { number } from 'prop-types';

import { gsap } from 'gsap';
import useComponentDidMount from 'hooks/useComponentDidMount';
import Item from './Item';

import cursorImage from 'images/footer.jpg';

import useArrayRef from 'hooks/useArrayRef';

import { WrapPosition } from 'components/WrapCursor';
import getElementPosition from 'utils/getElementPosition';

import s from './Ticker.scss';

const Ticker = ({ repeat }) => {
  const items = Array(repeat).fill('');

  const [refs, setRefs] = useArrayRef();
  const imageRef = useRef(null);
  const tween = useRef(null);

  const initAnimation = () => {
    if (tween.current) {
      tween.current.pause().kill();
    }

    const elements = refs.current;

    if (elements.length > 0) {
      const itemWidth = elements[0].offsetWidth;

      const distantion = itemWidth * repeat;

      gsap.set(elements, {
        x: i => i * itemWidth,
      });

      tween.current = gsap.to(elements, {
        duration: 20,
        ease: 'none',
        x: `+=${distantion}`,
        modifiers: {
          x: gsap.utils.unitize(x => {
            return (parseFloat(x) % distantion) - itemWidth;
          }),
        },
        repeat: -1,
      });
    }
  };

  useComponentDidMount(() => {
    initAnimation();

    window.addEventListener('resize', initAnimation);

    return () => {
      window.removeEventListener('resize', initAnimation);
      if (tween.current) tween.current.pause().kill();
    };
  });

  const animateIn = (image, pos) => {
    gsap.killTweensOf(image);

    gsap.set(image, {
      x: pos.x,
      y: pos.y,
      opacity: 0,
      duration: 0.2,
    });

    gsap.to(image, {
      opacity: 1,
      duration: 0.2,
    });
  };

  const animateOut = image => {
    gsap.killTweensOf(image);

    gsap.to(image, {
      opacity: 0,
      duration: 0.2,
    });
  };

  const handleMouseEnter = (e, yPos, offsetTop) => {
    const position = getElementPosition(e, imageRef.current, yPos, offsetTop);

    animateIn(imageRef.current, position);
  };

  const handleMouseMove = (e, yPos, offsetTop) => {
    const position = getElementPosition(e, imageRef.current, yPos, offsetTop);

    gsap.to(imageRef.current, {
      x: position.x,
      y: position.y,
      duration: 0.2,
    });
  };

  const handleMouseLeave = () => {
    animateOut(imageRef.current);
  };

  return (
    <>
      <WrapPosition
        isHidden
        as="div"
        className={s.root}
        onMouseEnter={handleMouseEnter}
        onMouseLeave={handleMouseLeave}
        onMouseMove={handleMouseMove}
      >
        <div className={s.inner}>
          {items.map((_, i) => (
            <Item
              className={s.item}
              ref={setRefs}
              // eslint-disable-next-line
              key={`tick-${i}`}
            />
          ))}
        </div>
      </WrapPosition>
      <div
        className={s.image}
        style={{ backgroundImage: `url(${cursorImage})` }}
        ref={imageRef}
      />
    </>
  );
};

Ticker.propTypes = {
  repeat: number,
};

Ticker.defaultProps = {
  repeat: 5,
};

export default Ticker;
