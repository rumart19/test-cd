import React from 'react';

import Svg from 'components/Svg';

import { WrapCursor } from 'components/WrapCursor';

import s from './Copyright.scss';

const Copyright = () => {
  return (
    <div className={s.root}>
      <WrapCursor as="a" href="mailto:press@togethxr.com" className={s.bold}>
        press@togethxr.com
      </WrapCursor>
      <WrapCursor as="a" href="/">
        Shipping & Handling
      </WrapCursor>
      <div className={s.info}>
        <div className={s.title}>Togethxr</div>
        <div className={s.copy}>
          <div className={s.icon}>
            <Svg name="copy" width="37" height="26" />
          </div>
          <div className={s.text}>Copyright 2021 LA / CA</div>
        </div>
      </div>
      <WrapCursor as="a" href="/">
        Privacy & Information
      </WrapCursor>
      <WrapCursor as="a" href="mailto:info@togethxr.com" className={s.bold}>
        info@togethxr.com
      </WrapCursor>
    </div>
  );
};

export default Copyright;
