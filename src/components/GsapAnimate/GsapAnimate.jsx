import React from 'react';
import PropTypes from 'prop-types';
import { gsap } from 'gsap';

import { getTimings, getAnimationProps } from './utils';

class GsapAnimate extends React.PureComponent {
  static propTypes = {
    children: PropTypes.any,
    duration: PropTypes.oneOfType([
      PropTypes.number,
      PropTypes.shape({
        in: PropTypes.number,
        out: PropTypes.number,
      }).isRequired,
    ]),
    delay: PropTypes.oneOfType([
      PropTypes.number,
      PropTypes.shape({
        in: PropTypes.number,
        out: PropTypes.number,
      }).isRequired,
    ]),
    className: PropTypes.string,
    isVisible: PropTypes.bool,
    inView: PropTypes.bool,
    animateOnMount: PropTypes.bool,
    onAnimationEnd: PropTypes.func,
    animations: PropTypes.object,
    as: PropTypes.any,
    onComplete: PropTypes.func,
    ease: PropTypes.string,
    innerRef: PropTypes.oneOfType([PropTypes.func, PropTypes.object]),
  };

  static defaultProps = {
    delay: {
      in: 0,
      out: 0,
    },
    duration: 900,
    animations: {
      in: 'fadeInUp',
      out: 'fadeOutUpSmall',
    },
    as: 'div',
    isVisible: false,
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    if (prevState.defOpacity === 0 && nextProps.isVisible) {
      return { defOpacity: null };
    }

    return null;
  }

  state = { defOpacity: 0 };

  componentDidMount() {
    const {
      duration,
      delay,
      animateOnMount,
      isVisible,
      inView,
      animations,
    } = this.props;

    const animated = animateOnMount || isVisible || inView;

    if (animated) {
      this.onAnimationTween(animations, duration, delay, animated);
    } else {
      const type = !(isVisible || inView) ? 'in' : 'out';
      const { start } = getAnimationProps(type, animations);
      gsap.set(this.nodeRef, {
        ...start,
        pointerEvents: isVisible || inView ? '' : 'none',
      });
    }
  }
  //
  componentDidUpdate(prevProps) {
    const { duration, delay, isVisible, animations, inView } = this.props;

    if (isVisible !== prevProps.isVisible || inView !== prevProps.inView) {
      this.onAnimationTween(animations, duration, delay, isVisible || inView);
    }
  }

  onAnimationTween = (animations, durationIn, delayIn, isVisible) => {
    const { onComplete, ease } = this.props;
    const type = isVisible ? 'in' : 'out';
    const { delay, duration } = getTimings(type, durationIn, delayIn);
    const { start, end } = getAnimationProps(type, animations);

    if (this.gsap) {
      this.gsap.kill();
      this.gsap = null;
    }

    this.gsap = gsap.fromTo(
      this.nodeRef,
      {
        ...start,
      },
      {
        duration,
        ...end,
        pointerEvents: isVisible ? '' : 'none',
        delay,
        clearProps: isVisible ? 'all' : '',
        ease: ease || (isVisible ? 'expo.out' : 'sine.inOut'),
        onComplete: () => {
          if (this.nodeRef) {
            gsap.set(this.nodeRef, { pointerEvents: isVisible ? '' : 'none' });
          }

          if (onComplete) {
            onComplete(isVisible);
          }
        },
      }
    );
  };

  getNodeRef = node => {
    const { innerRef } = this.props;
    this.nodeRef = node;

    if (innerRef) {
      innerRef.current = node;
    }
  };

  render() {
    const {
      children,
      duration,
      delay,
      className,
      isVisible,
      animateOnMount,
      animations,
      as,
      onComplete,
      innerRef,
      inView,
      ease,
      ...otherProps
    } = this.props;

    const As = as;
    return (
      <As ref={this.getNodeRef} className={className} {...otherProps}>
        {children}
      </As>
    );
  }
}

export default GsapAnimate;
