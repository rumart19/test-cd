const anim = {
  none: {
    start: {},
    end: {},
  },
  fadeIn: {
    start: {
      opacity: 0,
    },
    end: {
      opacity: 1,
    },
  },
  fadeOut: {
    start: {
      opacity: 1,
    },
    end: {
      opacity: 0,
    },
  },
  fadeInUp: {
    start: {
      yPercent: 100,
      opacity: 0,
    },
    end: {
      yPercent: 0,
      opacity: 1,
    },
  },
  fadeOutUp: {
    start: {
      yPercent: 0,
      opacity: 1,
    },
    end: {
      yPercent: -100,
      opacity: 0,
    },
  },
  fadeInDown: {
    start: {
      opacity: 0,
      yPercent: -100,
    },
    end: {
      opacity: 1,
      yPercent: 0,
    },
  },
  fadeOutDown: {
    start: {
      opacity: 1,
      yPercent: 0,
    },
    end: {
      opacity: 0,
      yPercent: 100,
    },
  },
  fadeInUpSmall: {
    start: {
      opacity: 0,
      y: 10,
    },
    end: {
      opacity: 1,
      y: 0,
    },
  },
  fadeInUp100: {
    start: {
      opacity: 0,
      y: 100,
    },
    end: {
      opacity: 1,
      y: 0,
    },
  },
  fadeOutUpSmall: {
    start: {
      opacity: 1,
      y: 0,
    },
    end: {
      opacity: 0,
      y: -10,
    },
  },
  fadeInDownSmall: {
    start: {
      opacity: 0,
      y: -10,
    },
    end: {
      opacity: 1,
      y: 0,
    },
  },
  slideInUp: {
    start: {
      yPercent: 100,
    },
    end: {
      yPercent: 0,
    },
  },
  slideOutUp: {
    start: {
      yPercent: 0,
    },
    end: {
      yPercent: -100,
    },
  },
  scaleIn: {
    start: {
      opacity: 0,
      scale: 1.3,
    },
    end: {
      opacity: 1,
      scale: 1,
    },
  },
  scaleOut: {
    start: {
      opacity: 1,
      scale: 1,
    },
    end: {
      opacity: 0,
      scale: 1.3,
    },
  },
  animationNone: {
    start: {
      y: 0,
    },
    end: {
      y: 0,
    },
  },
  blurIn: {
    start: {
      opacity: 0,
      filter: 'blur(200px)',
    },
    end: {
      opacity: 1,
      filter: 'blur(0px)',
    },
  },
  blurOut: {
    start: {
      opacity: 1,
      filter: 'blur(0px)',
    },
    end: {
      opacity: 0,
      filter: 'blur(200px)',
    },
  },
};

export const getTimings = (type, duration, delay) => {
  const durationTween = duration instanceof Object ? duration[type] : duration;
  const delayTween = delay instanceof Object ? delay[type] : delay;

  return {
    delay: delayTween / 1000,
    duration: durationTween / 1000,
  };
};

export const getAnimationProps = (type, animations) => {
  return anim[animations[type]];
};
