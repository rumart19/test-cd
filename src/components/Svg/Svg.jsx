import React, { useCallback } from 'react';
import classes from 'classnames';
import { string, any, func, oneOf } from 'prop-types';

import modsClasses from 'utils/modsClasses';

import arrow from './Icons/Arrow';
import arrowr from './Icons/ArrowRight';
import star from './Icons/Star';

import yt from './Icons/Yt';
import inst from './Icons/Inst';
import tiktok from './Icons/Tiktok';
import ghost from './Icons/Ghost';
import tweet from './Icons/Tweet';
import facebook from './Icons/Facebook';

import logo from './Icons/Logo';
import play from './Icons/Play';

import thxr from './Icons/Thxr';
import xo from './Icons/Xo';
import triangle from './Icons/Triangle';

import globe from './Icons/Globe';
import xicon from './Icons/Xicon';

import copy from './Icons/Copy';

import s from './Svg.scss';

const icons = {
  arrow,
  arrowr,
  star,
  yt,
  inst,
  ghost,
  tweet,
  facebook,
  tiktok,
  logo,
  play,
  thxr,
  xo,
  triangle,
  globe,
  xicon,
  copy,
};

const Svg = ({
  className,
  name,
  data,
  color,
  width,
  height,
  stroke,
  onClick,
}) => {
  if (!(name in icons)) throw new Error('There is no such icon in icons');
  const iconSize = {
    width,
    height,
  };
  const SVGIcon = icons[name];
  const mods = modsClasses(s, { color, stroke, name });
  const handleClick = useCallback(() => {
    if (data && onClick) onClick(data);
  }, [data, onClick]);

  return (
    <SVGIcon
      className={classes(className, s.root, mods)}
      {...iconSize}
      onClick={handleClick}
    />
  );
};

Svg.propTypes = {
  className: string,
  name: string.isRequired,
  data: any,
  width: string.isRequired,
  height: string.isRequired,
  stroke: oneOf(['dark', 'light', 'none']),
  color: oneOf(['dark', 'light', 'none']),
  onClick: func,
};

Svg.defaultProps = {
  className: '',
  data: undefined,
  color: 'dark',
  stroke: 'none',
  onClick: undefined,
};

export default Svg;
