import React from 'react';

export default props => (
  <svg viewBox="0 0 25 23" xmlns="http://www.w3.org/2000/svg" {...props}>
    <g fill="none" fillRule="evenodd">
      <path d="M2 1h21v21" />
      <path strokeLinecap="square" d="M22.597 1.333L2.403 21.667" />
    </g>
  </svg>
);
