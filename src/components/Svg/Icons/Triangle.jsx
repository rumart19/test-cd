import React from 'react';

export default props => (
  <svg viewBox="0 0 13 22" xmlns="http://www.w3.org/2000/svg" {...props}>
    <path d="M6.205 22L0 0h12.41z" fill="currentColor" fillRule="evenodd" />
  </svg>
);
