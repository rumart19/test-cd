import React from 'react';

export default props => (
  <svg viewBox="0 0 29 29" xmlns="http://www.w3.org/2000/svg" {...props}>
    <defs>
      <rect id="path-1" x="0" y="0" width="29" height="29" />
    </defs>
    <g stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
      <mask id="mask-2" fill="white">
        <use xlinkHref="#path-1" />
      </mask>
      <use fill="none" xlinkHref="#path-1" />
      <g mask="url(#mask-2)">
        <g transform="translate(1.000000, 5.500000)">
          <g>
            <path
              d="M18.0906233,9.5035365 L11.2961253,12.7049988 C11.0295124,12.8416117 10.1245306,12.6577923 10.1245306,12.3695462 L10.1245306,5.79853771 C10.1245306,5.50671541 11.0370226,5.32289597 11.3036355,5.46737662 L17.807488,8.8369228 C18.080109,8.9849797 18.3662485,9.3612016 18.0906233,9.5035365 Z M27,5.64261305 C27,2.5262656 24.3473895,0 21.0751857,0 L5.9240633,0 C2.6518595,0 0,2.5262656 0,5.64261305 L0,12.357387 C0,15.4744497 2.6518595,18 5.9240633,18 L21.0751857,18 C24.3473895,18 27,15.4744497 27,12.357387 L27,5.64261305 L27,5.64261305 Z"
              id="Shape"
              fill="currentColor"
              fillRule="nonzero"
            />
          </g>
        </g>
      </g>
    </g>
  </svg>
);
