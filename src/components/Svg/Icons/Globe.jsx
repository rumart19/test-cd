import React from 'react';

export default props => (
  <svg viewBox="0 0 23 26" xmlns="http://www.w3.org/2000/svg" {...props}>
    <g
      transform="translate(1 2)"
      fill="none"
      fillRule="evenodd"
      stroke="currentColor"
    >
      <ellipse cx="10.5" cy="11.5" rx="10.5" ry="11.5" />
      <ellipse
        transform="rotate(90 11 12)"
        cx="10.5"
        cy="11.5"
        rx="6.5"
        ry="10.5"
      />
      <path d="M1 12h21M11 1v22m5-21v19M5 2v18" strokeLinecap="square" />
    </g>
  </svg>
);
