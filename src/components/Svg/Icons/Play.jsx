import React from 'react';

export default props => (
  <svg viewBox="0 0 14 14" xmlns="http://www.w3.org/2000/svg" {...props}>
    <g strokeWidth="1" fillRule="evenodd">
      <g transform="translate(-1747.000000, -1145.000000)">
        <g id="Group-5" transform="translate(1730.000000, 1129.000000)">
          <polygon
            transform="translate(24.150000, 22.900000) rotate(-270.000000) translate(-24.150000, -22.900000) "
            points="24.15 16 31.05 29.8 17.25 29.8"
          />
        </g>
      </g>
    </g>
  </svg>
);
