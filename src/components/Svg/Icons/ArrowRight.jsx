import React from 'react';

export default props => (
  <svg viewBox="0 0 21 21" xmlns="http://www.w3.org/2000/svg" {...props}>
    <g stroke="#000" fill="none" fillRule="evenodd">
      <path d="M10.5 2.015l8.485 8.485-8.485 8.485" />
      <path strokeLinecap="square" d="M18.14 10.5H1.86" />
    </g>
  </svg>
);
