import React from 'react';
import * as PIXI from 'pixi.js';
import debounce from 'lodash.debounce';
import { Cull } from '@pixi-essentials/cull';

import s from './PixiApp.scss';
import ResizeObserver from 'rc-resize-observer';

class PixiApp extends React.PureComponent {
  componentDidMount() {
    this.width = window.innerWidth;
    this.height = window.__height;

    this.app = new PIXI.Application({
      width: this.width,
      height: this.height,
      transparent: true,
      autoResize: true,
      resolution: window.MAX_RESOLUTION,
    });

    this.app.ticker.stop();
    this.app.renderer.plugins.interaction.destroy();
    this.rootRef.appendChild(this.app.view);
    window.pixiApp = this.app;

    const cull = new Cull({ recursive: true, toggle: 'visible' });

    cull.add(this.app.stage);

    this.app.renderer.on('prerender', () => {
      cull.cull(this.app.renderer.screen);
    });

    window.addEventListener('resize', this.handleResize);
    window.addEventListener(
      'resize',
      debounce(() => {
        this.app.ticker.update();
      }, 500)
    );

    if (process.env.APP_ENV !== 'development') {
      console.clear();
    }
  }

  getRootNodeRef = node => {
    this.rootRef = node;
  };

  handleResize = () => {
    this.width = window.innerWidth;
    this.height = window.__height;
    this.app.view.style.width = `${this.width}px`;
    this.app.view.style.height = `${this.height}px`;
    this.app.renderer.resize(this.width, this.height);

    this.app.ticker.update();
  };

  render() {
    return (
      <ResizeObserver onResize={this.handleResize}>
        <div ref={this.getRootNodeRef} className={s.root} />
      </ResizeObserver>
    );
  }
}

export default PixiApp;
