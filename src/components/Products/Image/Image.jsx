import React from 'react';
import { string } from 'prop-types';
import classes from 'classnames';
import { Link } from 'react-router-dom';

import prods from 'images/prods.jpg';

import s from './Image.scss';
import { WrapPosition } from 'components/WrapCursor';

const Image = ({ className }) => {
  return (
    <WrapPosition
      as="div"
      cursorText="the fit kit"
      className={classes(s.root, className)}
      isCheckScroll
    >
      <div className={s.picture}>
        <img src={prods} alt="" />
      </div>
      <div className={s.label}>featured</div>
      <Link to="/" className={s.link} />
    </WrapPosition>
  );
};

Image.propTypes = {
  className: string,
};

export default Image;
