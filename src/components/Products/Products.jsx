import React from 'react';

import Image from './Image';
import Content from './Content';

import s from './Products.scss';

const Products = () => {
  return (
    <div className={s.root}>
      <Image className={s.image} />
      <Content className={s.content} />
    </div>
  );
};

export default Products;
