import React from 'react';
import { string } from 'prop-types';
import classes from 'classnames';

import { WrapCursor } from 'components/WrapCursor';

import s from './ProdButton.scss';

const ProdButton = ({ text, className }) => {
  return (
    <WrapCursor to="/" className={classes(s.root, className)}>
      <span>{text}</span>
    </WrapCursor>
  );
};

ProdButton.propTypes = {
  text: string.isRequired,
  className: string,
};

export default ProdButton;
