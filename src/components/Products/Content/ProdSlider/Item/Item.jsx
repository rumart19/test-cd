import React from 'react';

import classes from 'classnames';

import s from './Item.scss';
import { string } from 'prop-types';

const Item = React.forwardRef(({ className, url }, ref) => {
  return (
    <div className={classes(s.root, className)} ref={ref}>
      <img src={url} alt="" />
    </div>
  );
});

Item.propTypes = {
  className: string,
  url: string.isRequired,
};

Item.defaultProps = {
  className: '',
};

export default Item;
