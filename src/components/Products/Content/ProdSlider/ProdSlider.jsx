import React, { useRef } from 'react';
import { string } from 'prop-types';
import classes from 'classnames';

import { gsap } from 'gsap';
import useComponentDidMount from 'hooks/useComponentDidMount';

import Item from './Item';

import product1 from 'images/product.png';
import product2 from 'images/product-2.png';
import product3 from 'images/product-3.png';

import s from './ProdSlider.scss';

const ProdSlider = ({ className }) => {
  const tickerElements = useRef([]);
  const tween = useRef(null);

  const tickerElementsRefHandler = (e, i) => {
    tickerElements.current[i] = e;
  };

  const initAnimation = () => {
    if (tween.current) {
      tween.current.pause().kill();
    }

    const elements = tickerElements.current;

    if (elements.length > 0) {
      const style =
        elements[0].currentStyle || window.getComputedStyle(elements[0]);

      const gap = parseFloat(style.marginBottom);

      const itemHeight = elements[0].clientHeight + gap;
      const distantion = itemHeight * elements.length;

      gsap.set(elements, {
        y: i => i * itemHeight,
      });

      tween.current = gsap.to(elements, {
        duration: 24,
        ease: 'none',
        y: `-=${distantion}`,
        modifiers: {
          y: gsap.utils.unitize(y => {
            return y < 0
              ? (parseFloat(y) % distantion) + distantion - itemHeight
              : y - itemHeight;
          }),
        },
        repeat: -1,
      });
    }
  };

  useComponentDidMount(() => {
    initAnimation();

    window.addEventListener('resize', initAnimation);

    return () => {
      window.removeEventListener('resize', initAnimation);
      if (tween.current) tween.current.pause().kill();
    };
  });

  return (
    <div className={classes(s.root, className)}>
      <Item url={product1} ref={el => tickerElementsRefHandler(el, 0)} />
      <Item url={product2} ref={el => tickerElementsRefHandler(el, 1)} />
      <Item url={product3} ref={el => tickerElementsRefHandler(el, 2)} />
      <Item url={product1} ref={el => tickerElementsRefHandler(el, 3)} />
      <Item url={product2} ref={el => tickerElementsRefHandler(el, 4)} />
      <Item url={product3} ref={el => tickerElementsRefHandler(el, 5)} />
      <Item url={product1} ref={el => tickerElementsRefHandler(el, 6)} />
      <Item url={product2} ref={el => tickerElementsRefHandler(el, 7)} />
      <Item url={product3} ref={el => tickerElementsRefHandler(el, 8)} />
    </div>
  );
};

ProdSlider.propTypes = {
  className: string,
};

export default ProdSlider;
