import React from 'react';
import { string } from 'prop-types';
import classes from 'classnames';

import ProdSlider from './ProdSlider';
import ProdButton from './ProdButton';
import Ticker from 'components/Ticker/Ticker';

import s from './Content.scss';

const Content = ({ className }) => {
  return (
    <div className={classes(s.root, className)}>
      <div className={s.slider}>
        <ProdSlider />
        <ProdButton
          text="SHOP THE NEW  LTD.  EDITION TOGETHXR CREWNECK"
          className={s.btn}
        />
      </div>
      <Ticker className={s.ticker} duration={30} />
    </div>
  );
};

Content.propTypes = {
  className: string,
};

export default Content;
