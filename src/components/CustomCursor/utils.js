/* eslint-disable */

export const getPosition = e => {
  let x = 0;
  let y = 0;
  const evt = typeof e.originalEvent === 'undefined' ? e : e.originalEvent;

  if (
    e.type === 'touchstart' ||
    e.type === 'touchmove' ||
    e.type === 'touchend' ||
    e.type === 'touchcancel'
  ) {
    const touch = evt.touches[0] || evt.changedTouches[0];
    x = touch.pageX;
    y = touch.pageY;
  } else if (
    e.type === 'mousedown' ||
    e.type === 'mouseup' ||
    e.type === 'mousemove' ||
    e.type === 'mouseover' ||
    e.type === 'mouseout' ||
    e.type === 'mouseenter' ||
    e.type === 'mouseleave'
  ) {
    x = e.clientX;
    y = e.clientY;
  }

  return [x, y];
};

// Gets the mouse position
export const getMousePos = e => {
  let posx = 0;
  let posy = 0;
  // if (!e) e = window.event;
  if (e.pageX || e.pageY) {
    posx = e.pageX;
    posy = e.pageY;
  } else if (e.clientX || e.clientY) {
    posx =
      e.clientX +
      window.document.body.scrollLeft +
      document.documentElement.scrollLeft;
    posy =
      e.clientY +
      window.document.body.scrollTop +
      document.documentElement.scrollTop;
  }

  return { x: posx, y: posy };
};
