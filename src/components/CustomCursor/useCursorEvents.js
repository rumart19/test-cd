import { useCallback } from 'react';
import emitter from './cursorEmitter';

import { CURSOR_ENTER, CURSOR_LEAVE, CURSOR_CHANGE } from './constant';

export const cursorLeave = () => {
  emitter.emmit(CURSOR_LEAVE);
};

export const cursorEnter = (text, isHidden) => {
  emitter.emmit(CURSOR_ENTER, text, isHidden);
};

export const cursorChange = text => {
  emitter.emmit(CURSOR_CHANGE, text);
};

export const useCursorEvents = () => {
  const onCursorEnter = useCallback(cursorEnter, []);
  const onCursorLeave = useCallback(cursorLeave, []);
  const onCursorChange = useCallback(cursorChange, []);

  return [onCursorEnter, onCursorLeave, onCursorChange];
};
