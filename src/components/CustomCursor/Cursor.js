import { getPosition, getMousePos } from './utils';
import { clamp, round, mix } from 'utils/mathHelpers';
import { gsap } from 'gsap';

let functions = [];

export const globalCursorPosition = {
  x: 0,
  y: 0,
  distX: 0,
  distY: 0,
  isVisible: false,
  intersectionWithElement: false,
  onSubscribe: func => functions.push(func),
  onUnSubscribe: func => {
    functions = functions.filter(el => el !== func);
  },
  onUpdate: () => {
    functions.forEach(el => el());
  },
};

export default class Cursor {
  constructor(el, parent, amt = 0.15) {
    this.el = el;
    this.parent = parent;

    this.outWindowPosition = true;

    this.mouse = { x: 0, y: 0 };
    this.amt = amt;
    this.renderedStyles = {
      tx: { previous: 0, current: 0 },
      ty: { previous: 0, current: 0 },
    };

    this.isDisableInput = false;
    this.callbackOnRender = null;
    this.calculateSizePosition();

    window.addEventListener('mousemove', this.onMouseMoveEv);
    window.addEventListener('touchstart', this.onTouchEnd);
    window.addEventListener('touchend', this.onTouchEnd);
    window.addEventListener('resize', this.calculateSizePosition);
    window.document.addEventListener('mouseout', this.handleMouseWindowLeave);

    if (process.env.APP_ENV === 'development') {
      setTimeout(this.calculateSizePosition, 300);
    }

    gsap.set(this.el, { opacity: 0 });
  }

  calculateSizePosition = () => {
    this.bounds = this.el.getBoundingClientRect();

    this.parentBounds = this.parent
      ? this.parent.getBoundingClientRect()
      : {
          top: 0,
          left: 0,
          width: window.innerWidth,
          height: window.__height,
        };

    this.parentBounds.offsetTop = this.parent ? this.parent.offsetTop : 0;
    this.parentBounds.offsetLeft = this.parent ? this.parent.offsetLeft : 0;
  };

  enable = () => {
    gsap.ticker.add(this.render);
  };

  disable = () => {
    gsap.ticker.remove(this.render);
  };

  onEnableInput = () => {
    this.isDisableInput = false;
  };

  onDisableInput = () => {
    this.isDisableInput = true;
  };

  onTouchEnd = e => {
    const [clientX, clientY] = getPosition(e);
    this.mouse = { x: clientX, y: clientY };

    const centerX = this.bounds.width / 2;
    const centerY = this.bounds.height / 2;

    this.renderedStyles.tx.current = clamp(
      this.mouse.x - this.parentBounds.left - centerX,
      -centerX,
      this.parentBounds.width - centerX
    );
    this.renderedStyles.ty.current = clamp(
      this.mouse.y - this.parentBounds.top - centerY,
      -centerY,
      this.parentBounds.height - centerY
    );

    this.el.style.transform = `translate3d(${round(
      this.renderedStyles.tx.current,
      3
    )}px, ${round(this.renderedStyles.ty.current, 3)}px, 0)`;
  };

  onMouseMoveEv = e => {
    if (this.isDisableInput) return;
    this.mouse = getMousePos(e);

    if (this.outWindowPosition) {
      e = e ? e : window.event; // eslint-disable-line
      const from = e.relatedTarget || e.toElement;

      if (!(!from || from.nodeName === 'HTML')) {
        this.outWindowPosition = false;
        this.onChangeCursorVisible(true);
      }
    }
  };

  handleMouseWindowLeave = e => {
    e = e ? e : window.event; // eslint-disable-line
    const from = e.relatedTarget || e.toElement;

    if (!from || from.nodeName === 'HTML') {
      this.outWindowPosition = true;
      this.onChangeCursorVisible(false);
    }
  };

  onChangeCursorVisible = isVisible => {
    gsap.to(this.el, {
      opacity: isVisible ? 1 : 0,
      ease: 'power2.easeOut',
    });

    globalCursorPosition.isVisible = isVisible;
  };

  render = () => {
    const centerX = this.bounds.width / 2;
    const centerY = this.bounds.height / 2;

    const txPosition = this.mouse.x - this.parentBounds.left - centerX;
    const tyPosition = this.mouse.y - this.parentBounds.top - centerY;

    const maxTx = this.parentBounds.width - centerX;
    const maxTY = this.parentBounds.height - centerY;

    this.renderedStyles.tx.current = clamp(txPosition, -centerX, maxTx);
    this.renderedStyles.ty.current = clamp(tyPosition, -centerY, maxTY);

    Object.keys(this.renderedStyles).forEach(key => {
      this.renderedStyles[key].previous = mix(
        this.renderedStyles[key].previous,
        this.renderedStyles[key].current,
        this.amt
      );
    });

    const nextGlobX = round(this.renderedStyles.tx.previous + centerX, 3);
    const nextGlobY = round(this.renderedStyles.ty.previous + centerY, 3);

    globalCursorPosition.distX = nextGlobX - globalCursorPosition.x;
    globalCursorPosition.distY = nextGlobY - globalCursorPosition.y;
    globalCursorPosition.x = nextGlobX;
    globalCursorPosition.y = nextGlobY;

    globalCursorPosition.onUpdate();

    this.el.style.transform = `translate3d(${round(
      this.renderedStyles.tx.previous,
      3
    )}px, ${round(this.renderedStyles.ty.previous, 3)}px, 0)`;
  };

  destroy = () => {
    window.removeEventListener('mousemove', this.onMouseMoveEv);
    window.removeEventListener('resize', this.calculateSizePosition);
    window.document.removeEventListener(
      'mouseout',
      this.handleMouseWindowLeave
    );
    this.disable();
  };
}
