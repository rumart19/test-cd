import React, { useEffect, useRef, useState } from 'react';

import classes from 'classnames';
import emitter from './cursorEmitter';
import Cursor from './Cursor';

import gsap from 'gsap';

import useComponentDidMount from 'hooks/useComponentDidMount';

import { CURSOR_CHANGE, CURSOR_ENTER, CURSOR_LEAVE } from './constant';

import s from './CustomCursor.scss';

const CustomCursor = () => {
  const root = useRef(null);
  const icon = useRef(null);

  const [isHover, setIsHover] = useState(false);
  const [cursorText, setCursorText] = useState('');
  const [isHide, setIsHide] = useState(false);

  let cursor = null;

  const handleEnter = (newCursorText = '', isHidden) => {
    setIsHide(isHidden);
    setIsHover(true);
    setCursorText(prevText =>
      prevText !== newCursorText ? newCursorText : prevText
    );
  };

  const handleLeave = () => {
    setIsHide(false);
    setIsHover(false);
    setCursorText('');
  };

  const handleChange = (newCursorText = '') => {
    setCursorText(prevText =>
      prevText !== newCursorText ? newCursorText : prevText
    );
  };

  useEffect(() => {
    gsap.to(root.current, {
      opacity: isHide ? 0 : 1,
    });
  }, [isHide]);

  useComponentDidMount(() => {
    cursor = new Cursor(root.current);
    cursor.enable();

    emitter.addListener(CURSOR_ENTER, handleEnter);
    emitter.addListener(CURSOR_LEAVE, handleLeave);
    emitter.addListener(CURSOR_CHANGE, handleChange);

    return () => {
      if (cursor) cursor.destroy();
      emitter.removeListener(CURSOR_ENTER, handleEnter);
      emitter.removeListener(CURSOR_LEAVE, handleLeave);
      emitter.removeListener(CURSOR_CHANGE, handleChange);
    };
  });

  return (
    <div
      ref={root}
      className={classes(s.root, {
        [s.isHover]: isHover && cursorText === '',
        [s.isText]: isHover && cursorText !== '',
        [s.isHidden]: isHide,
      })}
    >
      <div ref={icon} className={s.icon}>
        {cursorText && (
          <div className={s.text}>
            <span dangerouslySetInnerHTML={{ __html: cursorText } /* eslint-disable-line */} />
          </div>
        )}
      </div>
    </div>
  );
};

export default CustomCursor;
