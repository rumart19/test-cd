import React, { useRef } from 'react';

// import { string } from 'prop-types';

import Svg from 'components/Svg';
import Slider from './Slider';

// import gsap from 'gsap';

import s from './AboutUs.scss';

const AboutUs = () => {
  const aboutRef = useRef(null);
  const iconRef = useRef(null);

  // const [percent] = useElement(aboutRef, idScroll);

  // const iconTl = gsap.timeline({ paused: true });

  // iconTl.to(iconRef.current, {
  //   rotate: '100deg',
  // });

  // useEffect(() => {
  //   iconTl.progress(percent);
  // }, [percent, iconTl]);

  return (
    <div className={s.root} ref={aboutRef}>
      <div className={s.inner}>
        <div className={s.left}>
          <div className={s.title}>
            <Svg
              name="star"
              color="dark"
              stroke="none"
              width="32px"
              height="29px"
            />
            <span>ABOUT US</span>
          </div>
          <div className={s.icon} ref={iconRef} />
          <div className={s.text}>
            <p>
              TOGETH
              <Svg name="xicon" width="32px" height="29px" />R IS A NEW MEDIA
              AND COMMERCE COMPANY FOUNDED BY FOUR OF THE WORLD’S GREATEST
              PROFESSIONAL ATHLETES: ALEX MORGAN, CHLOE KIM, SUE BIRD, AND
              SIMONE MANUEL.
            </p>
            <p>
              WITH A FOCUS ON RICH STORYTELLING ROOTED IN LIFESTYLE AND YOUTH
              CULTURE, TOGETHR IS AN UNAPOLOGETIC PLATFORM WHERE REPRESENTATION
              AND EQUALITY IS THE NORM.
            </p>
            <p>
              TOGETH
              <Svg name="xicon" width="32px" height="29px" />R IS WHERE CULTURE,
              ACTIVISM, LIFESTYLE, AND SPORTS CONVERGE. WE SHATTER THE OFTEN
              NARROW DEPICTIONS OF WOMEN IN THE MEDIA WITH CONTENT FEATURING A
              DIVERSE AND INCLUSIVE COMMUNITY OF GAME-CHANGERS, CULTURE-SHAPERS,
              THOUGHT LEADERS, AND BARRIER BREAKERS.
            </p>
          </div>
        </div>
        <div className={s.right}>
          <Slider />
        </div>
      </div>
    </div>
  );
};

// AboutUs.propTypes = {
//   idScroll: string,
// };

export default AboutUs;
