import React, { useEffect, useRef, useState, useCallback } from 'react';

import { string } from 'prop-types';

import Item from './Item';

import { WrapPosition } from 'components/WrapCursor';

import useComponentDidMount from 'hooks/useComponentDidMount';

import classes from 'classnames';

import aboutImages from 'stubs/about';

import { cursorChange } from 'components/CustomCursor/useCursorEvents';

import gsap from 'gsap';

import useArrayRef from 'hooks/useArrayRef';

import s from './Slider.scss';

const Slider = ({ className }) => {
  const [refs, setRef] = useArrayRef();

  const [items, setItems] = useState([]);
  const [length, setLength] = useState(0);

  const [activeIndex, setActiveIndex] = useState(0);
  const [disabled, setDisabled] = useState(false);
  const [slideTitle, setSlideTitle] = useState('');

  const refTitle = useRef(null);

  const tl = gsap.timeline({ paused: true });
  const textTl = gsap.timeline({ paused: true });

  const getCounter = useCallback(() => {
    return `${activeIndex + 1}/${aboutImages.length}`;
  }, [activeIndex]);

  const animateElement = (elem, prevElem, title, index, final = false) => {
    setDisabled(true);

    if (final) {
      tl.set(elem, {
        zIndex: 2,
      });
    }

    textTl
      .to(title, {
        ease: 'power2.inOut',
        duration: 0.2,
        opacity: 0,
        onComplete: () => {
          setSlideTitle(aboutImages[index].title);
        },
      })
      .to(title, {
        ease: 'power2.inOut',
        duration: 0.2,
        opacity: 1,
      })
      .play();

    tl.to(prevElem, {
      duration: 0.6,
      scale: 1.4,
      ease: 'power2.inOut',
    });

    tl.to(
      elem,
      {
        duration: 0.6,
        yPercent: 0,
        ease: 'power2.inOut',
        onComplete: () => {
          if (final) {
            gsap.set(elem, {
              zIndex: 1,
            });
          }

          gsap.set(prevElem, {
            scale: 1,
            yPercent: 100,
          });

          setDisabled(false);
        },
      },
      '-=0.6'
    );

    tl.play();
  };

  const initAnim = useCallback(() => {
    for (let i = 1; i < length; i += 1) {
      gsap.set(refs.current[i], {
        yPercent: 100,
        zIndex: 1,
      });
    }
  }, [refs, length]);

  const handleClick = () => {
    if (!disabled) {
      const next = activeIndex + 1;

      const isFinal = next === length;

      const nextEl = refs.current[isFinal ? 0 : next];
      const prevEl = refs.current[isFinal ? length - 1 : activeIndex];

      setActiveIndex(isFinal ? 0 : next);
      animateElement(
        nextEl,
        prevEl,
        refTitle.current,
        isFinal ? 0 : next,
        isFinal
      );
    }
  };

  useEffect(() => {
    cursorChange(getCounter());
  }, [activeIndex, getCounter]);

  useEffect(() => {
    setItems(aboutImages);
    setLength(aboutImages.length);
    setSlideTitle(aboutImages[0].title);
    initAnim();
  }, [items, length, initAnim]);

  useComponentDidMount(() => {
    return () => {
      if (tl) tl.kill();
      if (textTl) textTl.kill();
    };
  });

  const getKey = (id, index) => {
    return `${id}_${index}`;
  };

  return (
    <>
      <WrapPosition
        as="div"
        cursorText={getCounter()}
        className={classes(s.root, className)}
        onClick={handleClick}
      >
        <div className={s.header}>TOGETHXR</div>
        <div className={s.items}>
          {items.map((item, index) => (
            <Item data={item} ref={setRef} key={getKey(item.id, index)} />
          ))}
        </div>
        {items.length > 0 && (
          <div className={s.title} ref={refTitle}>
            {slideTitle}
          </div>
        )}
      </WrapPosition>
    </>
  );
};

Slider.propTypes = {
  className: string,
};

export default Slider;
