import React from 'react';

import s from './Item.scss';
import { object } from 'prop-types';

const Item = React.forwardRef(({ data }, ref) => {
  return (
    <div className={s.root} ref={ref}>
      <div className={s.image}>
        <img src={data.photo} alt="" />
      </div>
    </div>
  );
});

Item.propTypes = {
  data: object.isRequired,
};

export default Item;
