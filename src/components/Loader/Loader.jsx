import React, { useRef, useContext, memo, useEffect } from 'react';
import { string } from 'prop-types';

import useAction from 'hooks/useAction';

import { actions } from 'models/common/slice';

import LoaderContext from 'components/Loader/context';
import useLoadingImage from 'components/Loader/useLoadingImage';

const Loader = ({ as, ...props }) => {
  const AS = as;
  const ref = useRef(null);

  const setContentLoaded = useAction(actions.contentLoaded);
  const setPreloaderHiden = useAction(actions.preloaderHidden);

  useLoadingImage(ref);

  const { totalProgress } = useContext(LoaderContext);

  useEffect(() => {
    if (totalProgress >= 1) {
      setContentLoaded();
      setPreloaderHiden();
    }
  }, [totalProgress]); // eslint-disable-line

  return <AS {...props} ref={ref} />;
};

Loader.propTypes = {
  as: string,
};

export default memo(Loader);
