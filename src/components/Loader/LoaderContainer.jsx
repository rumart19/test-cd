import React, { useState, useMemo, useCallback, memo } from 'react';
import { any } from 'prop-types';

import LoaderContext from './context';

const LoaderContainer = ({ children }) => {
  const [progressDom, onSetDomProgress] = useState(0);
  const [lengthDom, onSetLengthDom] = useState(0);

  const totalProgress = useMemo(() => progressDom / lengthDom, [
    progressDom,
    lengthDom,
  ]);

  const handleSetDomLength = useCallback(count => {
    if (count !== 0) {
      onSetLengthDom(1);
    }
  }, []);

  return (
    <LoaderContext.Provider
      value={{
        onSetDomProgress,
        onSetLengthDom: handleSetDomLength,
        totalProgress,
      }}
    >
      {children}
    </LoaderContext.Provider>
  );
};

LoaderContainer.propTypes = {
  children: any,
};

export default memo(LoaderContainer);
