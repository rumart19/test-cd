import { useContext } from 'react';
import useComponentDidMount from 'hooks/useComponentDidMount';
import forEach from 'lodash.foreach';
import uniqBy from 'lodash.uniqby';

import LoaderContext from './context';
import useSelector from 'hooks/useSelector';

import { contentLoadedSelector } from 'models/common/selectors';

export default ref => {
  const isLoadedContent = useSelector(contentLoadedSelector);
  const { onSetDomProgress, onSetLengthDom } = useContext(LoaderContext);

  useComponentDidMount(() => {
    if (!isLoadedContent) {
      const loadedImages = {};
      const images = ref.current.querySelectorAll('img');
      const { length } = uniqBy(images, 'src');
      onSetLengthDom(length);

      forEach(images, img => {
        // eslint-disable-next-line
        img.onload = () => {
          loadedImages[img.src] = '';
          onSetDomProgress(Object.keys(loadedImages).length / length);
        };
      });
    }
  });
};
