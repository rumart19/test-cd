/* eslint-disable */
import React, { PureComponent } from 'react';
import { any, string, number, bool, func, object } from 'prop-types';
import WheelIndicator from 'wheel-indicator';
import normalizeWheel from 'normalize-wheel';
import classnames from 'classnames';
import { connect } from 'react-redux';

import {
  requestTimeout,
  clearRequestTimeout,
} from '@essentials/request-timeout';

import throttle from 'lodash.throttle';
import ResizeObserver from 'rc-resize-observer';
import { gsap } from 'gsap';

import swipeable from 'utils/swipeable';

import emitter from './scrollEmitter';

import { cursorLeave } from 'components/CustomCursor/useCursorEvents';

import { ScrollContext } from './scrollContext';

import createAction from 'utils/createAction';

import {
  pageScrollsSelector,
  isPopStateSelector,
} from 'models/common/selectors';

import { actions } from 'models/common/slice';

import { PageVisibleHoc } from 'components/PageWrapper';

import {
  CHANGE_SCROLL_POSITION,
  SCROLL_TO_BOTTOM,
  SCROLL_TO_TOP,
  SCROLL_TO,
  SCROLL_TO_CHILD,
  END_SCROLL,
  RESIZE_CONTAINER,
  DISABLE_SCROLL,
  ENABLE_SCROLL,
  CLEAR_SWIPE,
} from './constants';

import { clamp, mix, outClamp, round } from './utils';

import s from './CustomScroll.scss';

import { globalCursorPosition } from 'components/CustomCursor';

/* prettier-ignore */

@PageVisibleHoc

@connect(
  state => ({
    pageScrolls: pageScrollsSelector(state),
    isPopState: isPopStateSelector(state),
  }),
    {
      onSetPageScroll: createAction(actions.setPageScroll),
      onSetPopState: createAction(actions.setIsPopStateAction),
    }
)

class CustomScroll extends PureComponent {
  constructor(props) {
    super(props);

    this.childList = {};

    this.maxScroll = 0;
    this.currentScroll = 0;
    this.scrollY = 0;
    this.paused = true;

    this.finalBlend = 0.08;
    this.maxDif = 500;

    this.dopSize = 0;

    this.defDisable = false;
    this.initing = false;

    this.actionString = `${props.idScroll}-${CHANGE_SCROLL_POSITION}`;
    this.actionScrollToBottom = `${props.idScroll}-${SCROLL_TO_BOTTOM}`;
    this.actionScrollToTop = `${props.idScroll}-${SCROLL_TO_TOP}`;
    this.actionScrollTo = `${props.idScroll}-${SCROLL_TO}`;
    this.actionScrollToChild = `${props.idScroll}-${SCROLL_TO_CHILD}`;
    this.actionEndScroll = `${props.idScroll}-${END_SCROLL}`;
    this.actionResizeScroll = `${props.idScroll}-${RESIZE_CONTAINER}`;

    this.actionDisableScroll = `${props.idScroll}-${DISABLE_SCROLL}`;
    this.actionEnableScroll = `${props.idScroll}-${ENABLE_SCROLL}`;

    this.throttleChangeFialBland = throttle(this.changeFialBland, 40, {
      leading: false,
    });
  }

  componentDidMount() {
    this.windowHeight = window.__height;
    window.addEventListener('wheel', this.handleWheel, { passive: false });
    window.addEventListener('resize', this.handleResize);

    emitter.addListener(this.actionScrollToBottom, this.scrollToBottom);
    emitter.addListener(this.actionScrollToTop, this.scrollToTop);
    emitter.addListener(this.actionScrollToChild, this.scrollToChild);

    emitter.addListener(this.actionScrollTo, this.scrollTo);

    emitter.addListener(CLEAR_SWIPE, this.clearSwipe);

    emitter.addListener(this.actionDisableScroll, this.handleDisable);
    emitter.addListener(this.actionEnableScroll, this.handleEnable);
    
    if (this.props.isPageIn) {
      this.requestId = requestAnimationFrame(this.init);
    }
  }

  componentDidUpdate(prevProps) {
    const {
      startFrom,
      startFromPoint,
      isDisable,
    } = this.props;

    if (this.props.isPageIn !== prevProps.isPageIn && this.props.isPageIn) {
      this.init();
    }

    if (startFromPoint && prevProps.startFrom !== startFrom) {
      this.setStartFromPoint(startFrom);
    }

    if (isDisable !== prevProps.isDisable) {
      if (isDisable) {
        this.addTicker = false;
        gsap.ticker.remove(this.cb);
        emitter.emmit(this.actionEndScroll, this.currentScroll, this.maxScroll);
      } else {
        if (this.props.isPopState) {
          const percent = this.props.pageScrolls[this.props.idScroll];
          if (percent) {
            const position = this.maxScroll * -percent;
            this.currentScroll = position;
            this.scrollY = position;

            emitter.emmit(
              this.actionString,
              this.currentScroll,
              this.maxScroll,
              this.props.isInfinity,
              this.blend,
              1,
              true,
            );
          }
        }

        this.props.onSetPopState(false)
        if (!this.addTicker) {
          gsap.ticker.add(this.cb);
        }
      }
    }
  }

  componentWillUnmount() {
    cancelAnimationFrame(this.requestId);

    window.removeEventListener('wheel', this.handleWheel, { passive: false });
    window.removeEventListener('resize', this.handleResize);
    // window.removeEventListener('keydown', this.handleKeyDown);

    emitter.removeListener(this.actionScrollToBottom, this.scrollToBottom);
    emitter.removeListener(this.actionScrollToTop, this.scrollToTop);
    emitter.removeListener(this.actionScrollToChild, this.scrollToChild);

    emitter.removeListener(this.actionScrollTo, this.scrollTo);

    emitter.removeListener(this.actionDisableScroll, this.handleDisable);
    emitter.removeListener(this.actionEnableScroll, this.handleEnable);

    emitter.removeListener(CLEAR_SWIPE, this.clearSwipe);

    if (this.initing) {
      this.WheelIndicator.destroy();
      this.swipeable.destroy();
      gsap.ticker.remove(this.cb);

      if (this.props.savePositionOnUnmount) {
        this.props.onSetPageScroll({
          page: this.props.idScroll,
          position: Math.abs(this.currentScroll / this.maxScroll),
        });
      }
    }
  }

  setPointOnDidMount = () => {
    const percent = this.props.pageScrolls[this.props.idScroll];
    const position = percent ? this.maxScroll * -percent : 0;
    this.setStartFromPoint(position);
  };

  init = () => {
    this.WheelIndicator = new WheelIndicator({
      elem: window,
      callback: this.handleWheelIndicator,
    });

    this.setMaxScroll();

    //swipeable
    this.swipeable = swipeable(this.root, {
      trackTouch: true,
      deltaY: 20,
      swipestart: this.handleSwipStart,
      swiping: this.handleSwiping,
      swiped: this.handleSwiped,
    });
    //swipeable

    this.requestAnimation = requestTimeout(() => {
      this.addTicker = true;
      gsap.ticker.add(this.cb);
      this.setDefaultPositionScroll();
      
      emitter.emmit(
        this.actionResizeScroll,
        this.currentScroll,
        this.maxScroll
      );

      clearRequestTimeout(this.requestAnimation);
    }, 900);

    this.initing = true;
  };

  setMaxScroll = () => {
    this.windowHeight = window.__height;
    const wrapperSize = this.props.heightParrent
      ? this.root.parentElement.getBoundingClientRect().height
      : this.windowHeight;

    this.maxScroll = this.root.clientHeight - wrapperSize;
    if (this.maxScroll < 0) this.maxScroll = 0;

    emitter.emmit(this.actionResizeScroll, this.currentScroll, this.maxScroll);

    if (this.props.onCalcMaxScroll) {
      this.props.onCalcMaxScroll(this.maxScroll);
    }
  };

  setDefaultPositionScroll = () => {
    const { startFromPoint, startFromCenter, startFrom } = this.props;

    if (startFromCenter) {
      this.scrollY =
        this.windowHeight / 2 - (this.maxScroll + this.windowHeight) / 2;
      this.doneDist = this.scrollY;
    }

    if (startFromPoint && startFrom) {
      this.setStartFromPoint(startFrom);
    }
  };

  setStartFromPoint = (startFrom, maxCount = 5) => {
    this.scrollY = startFrom > 0 ? startFrom * -1 : startFrom;
    this.doneDist = this.scrollY;
    this.currentScroll = this.scrollY;
    let count = 0;

    const cbInterval = setInterval(() => {
      if (count === maxCount) {
        clearInterval(cbInterval);
        return;
      }

      emitter.emmit(
        this.actionString,
        this.currentScroll,
        this.maxScroll,
        this.props.isInfinity
      );
      count += 1;
    }, 30);
  };

  handleDisable = () => {
    this.defDisable = true;
  };

  handleEnable = () => {
    this.defDisable = false;
  };

  handleResize = () => {
    const per = this.currentScroll / this.maxScroll || 0;
    this.windowHeight = window.__height;
    const wrapperSize = this.props.heightParrent
      ? this.root.parentElement.getBoundingClientRect().height
      : this.windowHeight;

    this.maxScroll = this.root.clientHeight - wrapperSize;
    if (this.maxScroll < 0) this.maxScroll = 0;

    const newPositionScroll = outClamp(
      this.maxScroll * per,
      0,
      -this.maxScroll
    );

    this.currentScroll = newPositionScroll;
    this.scrollY = newPositionScroll;

    this.doneDist = this.scrollY;

    this.paused = false;
    this.cb();
    this.paused = true;
  };

  handleWheelIndicator = () => {
    const { isOpenMenu, idScroll, isDisable } = this.props;
    if ((isOpenMenu && idScroll !== 'Menu') || isDisable || this.defDisable)
      return;

    if (this.requestDistToEnd) {
      clearRequestTimeout(this.requestDistToEnd);
      this.requestDistToEnd = null;
    }


    this.maxDif = 500;
    this.blend = undefined;
    this.paused = false;
    this.doneDist = 0;
  };

  handleWheel = e => {
    const { isOpenMenu, idScroll, isDisable } = this.props;
    if ((isOpenMenu && idScroll !== 'Menu') || isDisable || this.defDisable)
      return;
    e.preventDefault();

    const normalized = normalizeWheel(e);
    const { pixelY, pixelX } = normalized;

    if (Math.abs(pixelY) > Math.abs(pixelX)) {

      const p = clamp(pixelY, -140, 140);
      let t = p * -1;
      t *= 1.03;
      this.scrollY += t;

      if (!this.props.isInfinity) {
        this.scrollY = outClamp(this.scrollY, 0, -this.maxScroll);
      }
    }
  };

  changeFialBland = () => {
    const minScroll = 500;
    const ggg = round(this.scrollY - this.currentScroll, 4);
    const d = clamp(ggg, -minScroll, minScroll);
    const nBlend = Math.abs(d) / minScroll;
    let gb = clamp(nBlend * 0.327, 0.09, 0.327);

    this.finalBlend = gb;
  };

  cb = () => {
    if (!this.paused) {
      const r1 = round(this.scrollY, 3);
      const r2 = round(this.currentScroll, 3);
      const prev = this.currentScroll;

      const absDifference = Math.abs(r1 - r2);
      const diffffMore = absDifference > this.maxDif;

      if (diffffMore) {
        const maxKKK = absDifference - this.maxDif;

        if (r1 > r2) {
          this.scrollY -= maxKKK;
        } else {
          this.scrollY += maxKKK;
        }
      }

      this.changeFialBland();

      let finalBlend = this.blend === undefined ? this.finalBlend : this.blend;

      if (
        (this.scrollY >= 0 || this.scrollY <= -this.maxScroll) &&
        this.props.withTopMagic &&
        !this.props.isDesktop
      ) {
        const minDistGt = clamp(
          Math.abs(this.scrollY - this.currentScroll) /
          (this.windowHeight * 0.158),
          0,
          1
        );
        finalBlend = mix(0.18, finalBlend, minDistGt);
      }

      this.currentScroll += (this.scrollY - this.currentScroll) * finalBlend;
      this.currentScroll = round(this.currentScroll, 6);

      this.doneDist += this.currentScroll - prev;

      const dir = prev - this.currentScroll > 0 ? 1 : -1;

      emitter.emmit(
        this.actionString,
        this.currentScroll,
        this.maxScroll,
        this.props.isInfinity,
        finalBlend,
        dir
      );

      const lostDist = round(this.scrollY - this.currentScroll, 4);

      if (Math.abs(lostDist) <= 0.001 && !this.requestDistToEnd) {
        this.requestDistToEnd = requestTimeout(() => {
          emitter.emmit(this.actionEndScroll, this.currentScroll, this.maxScroll);
          this.paused = true;

          clearRequestTimeout(this.requestDistToEnd);
        }, 400);
      }
    }
  };

  scrollTo = (nextPosition, customBlend = 0.1) => {
    this.setStartFromPoint(0);
  };

  scrollToTop = (duration = 4) => {
    if (Math.abs(this.currentScroll) < 30) {
      return;
    }

    this.paused = false;
    this.doneDist = 0;
    
    cursorLeave();

    gsap.to(this, {
      duration,
      currentScroll: 0,
      ease: 'power4.inOut',
      onUpdate: () => {
        this.scrollY = this.currentScroll;
        this.paused = true;
        emitter.emmit(
          this.actionString,
          this.currentScroll,
          this.maxScroll,
          this.props.isInfinity,
        );
      },
      onComplete: () => {
        this.paused = true;
      },
    });
  };

  scrollToChild = childHash => {
    const currInst = this.childList[childHash];
    const offset = currInst.props.offset || 0;

    const positionChildScroll = outClamp(
      -currInst.defaultPosition - offset,
      0,
      -this.maxScroll
    );

    const distToChild = Math.abs(this.currentScroll) - Math.abs(positionChildScroll);

    if (Math.abs(distToChild) < 30) {
      return;
    }

    this.paused = false;
    this.doneDist = 0;

    cursorLeave();

    gsap.to(this, {
      duration: 2,
      currentScroll: positionChildScroll,
      ease: 'power3.inOut',
      onUpdate: () => {
        this.scrollY = this.currentScroll;
        this.paused = true;
        emitter.emmit(
          this.actionString,
          this.currentScroll,
          this.maxScroll,
          this.props.isInfinity
        );
      },
      onComplete: () => {
        this.paused = true;
      },
    });
  };

  scrollToBottom = (blend = 0.28) => {
    this.paused = false;
    this.doneDist = 0;
    this.blend = blend;
    this.scrollY = -this.maxScroll;
    this.cb();
  };

  clearSwipe = () => {
    this.swipeable.clear();
    this.swipeable.onMouseUp();
  }

  handleSwipStart = () => {
    if (this.tweenStoper) this.tweenStoper.kill();
    const stopFactor = { x: 10 };
    this.tweenStoper = gsap.to(stopFactor, {
      duration: 5,
      x: 1,
      ease: 'circ.out',
      onUpdate: () => {
        const dist = this.scrollY - this.currentScroll;
        const difX = dist / stopFactor.x;
        this.scrollY = this.scrollY - difX;
        this.scrollY = outClamp(this.scrollY, 0, -this.maxScroll);
      },
    });
  };

  handleSwiping = ({ deltaY }) => {
    if (this.tweenStoper) this.tweenStoper.kill();

    const { isDisable } = this.props;
    if (isDisable || this.defDisable) return;

    if (!this.isSwiped) {
      this.swipedStart = this.currentScroll;
      this.paused = false;
      this.isSwiped = true;
      this.blend = 0.95;
      this.maxDif = Math.max(window.__height * 3, 1200);
    }

    this.scrollY = this.swipedStart - deltaY;

    const offset = this.props.disableRubberbands ? 0 : 200;

    if (!this.props.isInfinity) {
      this.scrollY = outClamp(this.scrollY, offset, -(this.maxScroll + offset));
    }
  };

  handleSwiped = ({ velocity, deltaY, difSwipeFrameY  }) => {
    const { isDisable } = this.props;
    if (isDisable || this.defDisable) return;
    if (!this.isSwiped) return;

    this.blend = 0.054;
    this.isSwiped = false;

    const nextDelta = deltaY + difSwipeFrameY * 4 * (velocity * 4.8);

    if (deltaY !== 0) {
      this.scrollY = this.swipedStart - nextDelta;
    }

    if (this.currentScroll > 0 || this.currentScroll < -this.maxScroll) {
      this.blend = 0.11;
    }

    if (!this.props.isInfinity) {
      this.scrollY = outClamp(this.scrollY, 0, -this.maxScroll);
    }
  };

  createRoot = node => {
    this.root = node;

    if (this.props.innerRef) {
      this.props.innerRef.current = node;
    }
  };

  addChild = (child, hash) => {
    this.childList[hash] = child;
  };

  removeChild = (hash) => {
    delete this.childList[hash];
  }

  getPosition = () => {
    return this.currentScroll;
  }

  render() {
    const { children, className, tag, isRelative } = this.props;
    const Tag = tag;
    return (
      <ResizeObserver onResize={this.setMaxScroll}>
        <Tag
          className={classnames(className, s.root, {[s.isRelative]: isRelative})}
          ref={this.createRoot}
        >
          <ScrollContext.Provider value={{
            addChild: this.addChild,
            removeChild: this.removeChild,
            getPosition: this.getPosition,
          }}>
            {children}
          </ScrollContext.Provider>
        </Tag>
      </ResizeObserver>
    );
  }
}

CustomScroll.propTypes = {
  children: any,
  className: string,
  isInfinity: bool,
  startFromCenter: bool,
  startFromPoint: bool,
  startFrom: number,
  idScroll: string.isRequired,
  isDisable: bool,
  isDesktop: bool,
  onCalcMaxScroll: func,
  tag: string,
  innerRef: object,
  isRelative: bool,
};

CustomScroll.defaultProps = {
  tag: 'div',
  isRelative: true,
};

export default CustomScroll;
