import React, { createRef } from 'react';
import { string, number, bool, any } from 'prop-types';
import classnames from 'classnames';
import debounce from 'lodash.debounce';
import throttle from 'lodash.throttle';
import uniqueId from 'lodash.uniqueid';

import emitter from './scrollEmitter';

import {
  CHANGE_SCROLL_POSITION,
  RESIZE_CONTAINER,
  END_SCROLL,
  CHANGE_VIEW_AREA,
} from './constants';

import { ScrollContext } from './scrollContext';

import { stickyScrolll } from './utilsChildScroll';
import { round, clamp } from 'utils/mathHelpers';

/* eslint-disable */

import styles from 'components/CustomScroll/CustomScroll.scss';

export const ChildScrollContext = React.createContext('');

class ChildScroll extends React.PureComponent {
  static contextType = ScrollContext;
  constructor(props) {
    super(props);
    this.root = createRef();
    this.isResize = false;
    this.screenSpringFactor = 1;
    this.currentPosition = 0;
    this.scrollPosition = 0;

    this.stateSkew = { y: 0 };

    this.defaultPosition = 0;
    this.defaultBottomPosition = 0;
    this.height = 0;

    this.visibility = true;

    this.actionResizeContainerListener = `${props.idListener}-${RESIZE_CONTAINER}`;
    this.actionListener = `${props.idListener}-${CHANGE_SCROLL_POSITION}`;
    this.actionEndListener = `${props.idListener}-${END_SCROLL}`;
    this.childId = uniqueId('scrollChild_');

    this.actionChildScroll = `${this.childId}-${CHANGE_SCROLL_POSITION}`;
    this.actionChildResize = `${this.childId}-${RESIZE_CONTAINER}`;
    this.actionChildViewArea = `${this.childId}-${CHANGE_VIEW_AREA}`;

    this.childActions = {
      scroll: this.actionChildScroll,
      resize: this.actionChildResize,
      viewArea: this.actionChildViewArea,
    };

    this.handleDebounceResize = debounce(this.handleResize, 200);
    this.handleThrotleResize = throttle(this.handleTimeResize, 50);
    this.handleThrotleChaneScroll = throttle(this.handleChangeScroll, 17);
  }

  componentDidMount() {
    const { hash } = this.props;
    if (hash) {
      this.context.addChild(this, hash);
    }
    this.innerHeight = window.__height;
    window.addEventListener('resize', this.handleDebounceResize);
    window.addEventListener('resize', this.handleThrotleResize);
    
    emitter.addListener(this.actionListener, this.handleChangeScroll);
    emitter.addListener(this.actionEndListener, this.handleEndScroll);
    emitter.addListener(
      this.actionResizeContainerListener,
      this.handleResizeContainer
    );
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.handleDebounceResize);
    window.removeEventListener('resize', this.handleThrotleResize);
    emitter.removeListener(this.actionListener, this.handleChangeScroll);
    emitter.removeListener(this.actionEndListener, this.handleEndScroll);
    emitter.removeListener(
      this.actionResizeContainerListener,
      this.handleResizeContainer
    );
  }

  handleResizeContainer = () => {
    if (!this.root.current) return;

    this.root.current.style.transform = `translate3d(${this.props.transformX}, 0, 0)`;
    const { y, height } = this.root.current.getBoundingClientRect();
    this.innerHeight = window.__height;
    this.defaultPosition = y;
    this.defaultBottomPosition = y + height;
    this.height = height;
    emitter.emmit(this.actionChildResize, this.currentPosition);
    this.root.current.style.transform = `translate3d(${this.props.transformX}, ${this.currentPosition}px, 0)`;

    const screenPositionY = this.defaultPosition + this.currentPosition;
    const screenPositionBottom =
      this.defaultBottomPosition + this.currentPosition;

    this.outViewArea =
      screenPositionY > this.innerHeight * 1.3 ||
      screenPositionBottom < this.innerHeight * -0.3;

    if (this.outViewArea) {
      this.visibility = false;
    } else {
      this.visibility = true;
    }
  };

  handleTimeResize = () => {
    this.isResize = true;
    this.innerHeight = window.__height;
  };

  handleResize = () => {
    this.handleResizeContainer();
    this.isResize = false;
  };

  handleEndScroll = () => {};

  // prettier-ignore
  handleChangeScroll = (scrollPosition, maxScroll, isInfinity, finalBlend, dir, isFast) => {
    const { springFactorFromWindow, sticky } = this.props;
    let springFactor = springFactorFromWindow ? this.screenSpringFactor : this.props.springFactor;
    if (this.isResize || isFast) springFactor = 1;

    let directionMove = Math.abs(this.scrollPosition) - Math.abs(scrollPosition) > 0 ? -1 : 1;
    if (scrollPosition > 0) {
      directionMove = Math.abs(scrollPosition) - Math.abs(this.scrollPosition) > 0 ? -1 : 1;
    }

    const dScrollPosition = (scrollPosition - this.scrollPosition) * springFactor;
    this.currentPosition = (this.isResize || isFast) ? scrollPosition : this.currentPosition + dScrollPosition;
    this.scrollPosition = (this.isResize || isFast) ? scrollPosition : this.scrollPosition + dScrollPosition;

    const screenList = maxScroll + this.innerHeight;

    const screenPositionY = this.defaultPosition + this.currentPosition;
    const screenPositionBottom =
      this.defaultBottomPosition + this.currentPosition;

    if (sticky) {
      const [nextStickyPosition] = stickyScrolll(
        this.scrollPosition,
        {
          start: 0,
          distance: maxScroll + 300,
        },
        this.defaultPosition,
        1
      );

      this.currentPosition = nextStickyPosition;
    }

    if (this.props.paralax) {
      const { speed } = this.props;
      const area = this.props.area || { top: 0, bottom: this.innerHeight };

      const minValue = (this.defaultPosition + area.top + this.height) * -1;
      const maxValue = (this.defaultPosition - area.bottom) * -1;

      let positionInArea = clamp(this.scrollPosition, minValue, maxValue);

      if (this.defaultPosition > area.bottom) {
        positionInArea -= maxValue;
      }

      const add2 = (positionInArea - positionInArea * speed) * -1;

      this.currentPosition = this.scrollPosition + add2;
    }

    if (!this.outViewArea || isFast) {
      this.root.current.style.transform = `translate3d(${
        this.props.transformX
      }, ${round(this.currentPosition, 2)}px, 0)`;

      emitter.emmit(this.actionChildScroll, this.currentPosition);

      if (!this.visibility) {

        emitter.emmit(this.actionChildViewArea, true);

        this.visibility = true;
      }
    }

    if (this.outViewArea && this.visibility) {
      this.visibility = false;

      emitter.emmit(this.actionChildViewArea, false);
    }

    this.outViewArea =
      screenPositionY > this.innerHeight * 1.3 ||
      screenPositionBottom < this.innerHeight * -0.3;

  };

  calculateSpringFactorFromScreen = (currentPosition, scrollPosition) => {
    let directionMove =
      Math.abs(currentPosition) - Math.abs(scrollPosition) > 0 ? -1 : 1;
    let delta = Math.abs(Math.abs(scrollPosition) - Math.abs(currentPosition));

    if (scrollPosition > 0) {
      directionMove =
        Math.abs(scrollPosition) - Math.abs(currentPosition) > 0 ? -1 : 1;
      delta = Math.abs(Math.abs(currentPosition) - Math.abs(scrollPosition));
    }

    const absDelta = Math.abs(clamp(delta / currentPosition, -1, 1));
    // в какой части есть середина
    const difScreen = (this.y + this.height / 2) / this.innerHeight;
    const percentOnScreen = clamp(1 - difScreen, 0, 1);
    const percentOnScreenBottom = clamp(difScreen, 0, 1);
    const testMax =
      directionMove === -1 ? 1 - percentOnScreen : 1 - percentOnScreenBottom;
    // магическая формула
    this.screenSpringFactor = testMax * testMax * Math.sqrt(absDelta) + 0.12;
  };

  createRef = node => {
    this.root.current = node;
  };

  render() {
    const {
      idListener,
      children,
      className,
      springFactor,
      withSkew,
      as,
      innerRef,
      hash,
      springFactorFromWindow,
      sticky,
      paralax,
      transformX,
      speed,
      ...otherProps
    } = this.props;

    const Tag = as;
    const refProps =
      typeof as == 'string'
        ? {
            ref: this.root,
          }
        : {
            innerRef: this.createRef,
          };

    return (
      <Tag
        className={classnames(className, styles.child)}
        {...refProps}
        {...otherProps}
      >
        <ChildScrollContext.Provider value={this.childActions}>
          {children}
        </ChildScrollContext.Provider>
      </Tag>
    );
  }
}

ChildScroll.propTypes = {
  idListener: string.isRequired,
  children: any,
  className: string,
  springFactor: number,
  withSkew: bool,
  paralax: bool,
  as: any,
  hash: string,
  transformX: string,
  springFactorFromWindow: bool,
};

ChildScroll.defaultProps = {
  springFactorFromWindow: false,
  springFactor: .7,
  transformX: '0',
  as: 'div',
};

export default ChildScroll;
