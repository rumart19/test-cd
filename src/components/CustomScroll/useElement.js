import {
  useContext,
  useRef,
  useMemo,
  useState,
  useEffect,
  useCallback,
} from 'react';
import useComponentDidMount from 'hooks/useComponentDidMount';
import debounce from 'lodash.debounce';

import { globalCursorPosition } from 'components/CustomCursor';

import { ScrollContext } from './scrollContext';
import emitter from './scrollEmitter';

import {
  CHANGE_SCROLL_POSITION,
  RESIZE_CONTAINER,
  ELEMENT_IN_VIEW,
} from 'components/CustomScroll/constants';

export default (ref, idListener, headerHeight) => {
  const [isTop, setIsTop] = useState(false);
  const [inView, setInView] = useState(false);
  const [progress, setProgress] = useState(0);
  const [yPos, setYPos] = useState(0);
  const [cursorIn, setCursorIn] = useState(false);

  const context = useContext(ScrollContext);
  const Element = useRef({
    isResize: false,
    screenSpringFactor: 1,
    currentPosition: 0,
    scrollPosition: 0,
    defaultPosition: 0,
    defautltLeftPosition: 0,
    defaultRightPosition: 0,
    defaultBottomPosition: 0,
    innerHeight: 0,
    height: 0,
    x: 0,
    y: 0,
    width: 0,
    visibility: false,
    inViewTop: false,
    isTopVisible: false,
    stikyDist: 0,
    inView: false,
    threshold: 0.15,
    viewportHeight: window.innerHeight,
  });

  const events = useMemo(
    () => ({
      actionResizeContainer: `${idListener}-${RESIZE_CONTAINER}`,
      actionScroll: `${idListener}-${CHANGE_SCROLL_POSITION}`,
      actionInView: `${idListener}-${ELEMENT_IN_VIEW}`,
    }),
    [idListener]
  );

  const detectInView = useCallback(() => {
    // prettier-ignore
    const { currentPosition, defaultPosition, defaultBottomPosition, innerHeight, viewportHeight, isTopVisible, y } = Element.current;

    const screenPositionY = defaultPosition + currentPosition;
    const screenPositionBottom = defaultBottomPosition + currentPosition;

    // prettier-ignore
    const outViewArea = screenPositionY > (innerHeight) || screenPositionBottom < 0;

    const distance = -currentPosition + viewportHeight - defaultPosition;

    const percentage = Math.round(
      distance / ((viewportHeight + innerHeight) / 100)
    );

    setProgress(Math.min(1, Math.max(0, percentage / 100)));

    if (outViewArea) {
      Element.current.visibility = false;
      Element.current.inView = false;
      setInView(false);
    } else {
      Element.current.visibility = true;
      Element.current.inView = true;
      setInView(true);
    }

    const outViewAreaHeight = headerHeight ? y - headerHeight >= 0 : y >= 0;

    if (outViewAreaHeight) {
      if (isTopVisible) {
        Element.current.isTopVisible = false;

        if (Element.current.inViewTop) {
          Element.current.inViewTop = false;
          setIsTop(false);
        }
      }
    } else if (!isTopVisible) {
      Element.current.isTopVisible = true;

      if (!Element.current.inViewTop) {
        Element.current.inViewTop = true;
        setIsTop(true);
      }
    }
  }, [setInView, setProgress, setIsTop, headerHeight]);

  const detectCursorIn = useCallback(() => {
    const {
      defaultPosition,
      defaultBottomPosition,
      defaultRightPosition,
      defaultLeftPosition,
      currentPosition,
    } = Element.current;

    const { y, x } = globalCursorPosition;
    const realCursorPos = { y: y + -currentPosition, x };

    if (
      realCursorPos.y >= defaultPosition &&
      realCursorPos.y <= defaultBottomPosition &&
      realCursorPos.x <= defaultRightPosition &&
      realCursorPos.x >= defaultLeftPosition
    ) {
      setCursorIn(true);
    } else {
      setCursorIn(false);
    }
  }, [setCursorIn]);

  const detectYPos = useCallback(() => {
    const { currentPosition } = Element.current;

    setYPos(currentPosition);
  }, [setYPos]);

  const handleResize = useCallback(() => {
    if (!ref) return;

    const { y, x, left, height, width } = ref.current.getBoundingClientRect();
    const cPosition = context.getPosition();

    Element.current = {
      ...Element.current,
      currentPosition: cPosition,
      scrollPosition: cPosition,
      innerHeight: window.__height,
      defaultPosition: y - cPosition,
      defaultBottomPosition: y - cPosition + height,
      defaultLeftPosition: left,
      defaultRightPosition: left + width,
      x,
      y,
      width,
      height,
      isResize: true,
    };

    detectInView();
  }, [detectInView, ref]);

  const handleDebResize = useCallback(() => {
    Element.current.isResize = false;
  }, [Element]);

  // prettier-ignore
  const handleScroll = useCallback((scrollPosition, maxScroll, inf, blend, dir, isFast) => {
      const { isResize, currentPosition, scrollPosition: elScrollPosition } = Element.current;
      const springFactor = 0.7;

      const dScrollPosition = (scrollPosition - elScrollPosition) * springFactor;
      Element.current.currentPosition = (isResize || isFast) ? scrollPosition : currentPosition + dScrollPosition;
      Element.current.scrollPosition = (isResize || isFast) ? scrollPosition : elScrollPosition + dScrollPosition;
      Element.current.y = Element.current.defaultPosition + Element.current.currentPosition;

      detectInView();
      detectYPos();
      detectCursorIn();
    },
    [detectInView, detectYPos, detectCursorIn]
  );

  useComponentDidMount(() => {
    return () => {};
  });

  useEffect(() => {
    const debounceResize = debounce(handleDebResize, 200);

    emitter.addListener(events.actionResizeContainer, handleResize);
    emitter.addListener(events.actionResizeContainer, debounceResize);
    emitter.addListener(events.actionScroll, handleScroll);

    window.addEventListener('resize', handleResize);
    window.addEventListener('resize', debounceResize);

    handleResize();
    const firstTimerResize = setTimeout(handleResize, 100);

    return () => {
      clearTimeout(firstTimerResize);
      emitter.removeListener(events.actionResizeContainer, debounceResize);
      emitter.removeListener(events.actionResizeContainer, handleResize);
      emitter.removeListener(events.actionScroll, handleScroll);

      window.removeEventListener('resize', handleResize);
      window.removeEventListener('resize', debounceResize);
    };
  }, [events, handleResize, handleScroll, handleDebResize]);

  return [
    {
      progress,
      offsetTop: Element.current.defaultPosition,
      cursorIn,
      yPos,
      inView,
      isTop,
    },
  ];
};
