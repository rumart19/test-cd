const clamp = (x, minVal, maxVal) => {
  return Math.min(Math.max(x, minVal), maxVal);
};

export function stickyScrolll(
  position,
  stickySettings,
  startElement,
  scrollDirection
) {
  const { start, distance } = stickySettings;
  let distToSticky =
    startElement < start
      ? start + (startElement + position)
      : startElement + position - start;

  if (scrollDirection === -1) {
    distToSticky = start - (startElement + position);
  }

  const stikystop = clamp(distToSticky, (distance + start) * -1, 0);
  const newPosition = position - stikystop * scrollDirection;
  return [newPosition, stikystop];
}
