import { ChildScrollContext } from './ChildScroll';
import React from 'react';

const ChildScrollActionHoc = WrappedComponent => {
  const HOC = props => (
    <ChildScrollContext.Consumer>
      {childActions => (
        <WrappedComponent {...props} childActions={childActions} />
      )}
    </ChildScrollContext.Consumer>
  );

  const getDisplayName = WrappedComponentIn =>
    WrappedComponentIn.displayName || WrappedComponentIn.name || 'Component';

  HOC.displayName = `ChildScrollActionHoc(${getDisplayName(WrappedComponent)})`;

  return React.memo(HOC);
};

export default ChildScrollActionHoc;
