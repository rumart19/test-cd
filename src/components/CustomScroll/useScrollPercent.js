import { useEffect, useMemo, useState } from 'react';

import { round } from 'utils/mathHelpers';
import emitter from './scrollEmitter';

import {
  CHANGE_SCROLL_POSITION,
  RESIZE_CONTAINER,
} from 'components/CustomScroll/constants';

export default idListener => {
  const [percent, setPercent] = useState(0);
  const events = useMemo(
    () => ({
      actionResizeContainer: `${idListener}-${RESIZE_CONTAINER}`,
      actionScroll: `${idListener}-${CHANGE_SCROLL_POSITION}`,
    }),
    [idListener]
  );

  useEffect(() => {
    const handleScroll = (currentScroll, maxScroll) => {
      const newPercent = round(
        Math.abs(Math.ceil(currentScroll) / maxScroll),
        2
      );

      setPercent(newPercent);
    };

    emitter.addListener(events.actionScroll, handleScroll);
    emitter.addListener(events.actionResizeContainer, handleScroll);

    return () => {
      emitter.removeListener(events.actionScroll, handleScroll);
      emitter.removeListener(events.actionResizeContainer, handleScroll);
    };
  }, [events]);

  return percent;
};
