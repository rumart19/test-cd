export const mix = (x, y, a) => {
  return x * (1 - a) + y * a;
};

export const clamp = (x, minVal, maxVal) => {
  return Math.min(Math.max(x, minVal), maxVal);
};

export const outClamp = (x, minVal, maxVal) => {
  return Math.max(Math.min(x, minVal), maxVal);
};

export const round = (t, i) => {
  const newI = i !== 0 ? 10 ** i : 1e3;
  return Math.round(t * newI) / newI;
};
