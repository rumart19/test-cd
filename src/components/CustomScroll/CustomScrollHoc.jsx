/* eslint-disable */
import React, { useCallback, memo } from 'react';
import emitter from './scrollEmitter';

import {
  SCROLL_TO_BOTTOM,
  SCROLL_TO_TOP,
  SCROLL_TO,
  SCROLL_TO_CHILD,
  DISABLE_SCROLL,
  ENABLE_SCROLL,
} from './constants';

const CustomScrollHoc = WrappedComponent => {
  const HOC = props => {
    const handleScrollTop = useCallback((idScroll, blend) => {
      emitter.emmit(`${idScroll}-${SCROLL_TO_TOP}`, blend);
    }, []);

    const handleScrollTo = useCallback((idScroll, posY) => {
      emitter.emmit(`${idScroll}-${SCROLL_TO}`, posY);
    }, []);

    const handleScrollBottom = useCallback((idScroll, blend) => {
      emitter.emmit(`${idScroll}-${SCROLL_TO_BOTTOM}`, blend);
    }, []);

    const handleScrollToChild = useCallback((idScroll, childHash) => {
      emitter.emmit(`${idScroll}-${SCROLL_TO_CHILD}`, childHash);
    }, []);

    const handleDisableScroll = useCallback(idScroll => {
      emitter.emmit(`${idScroll}-${DISABLE_SCROLL}`);
    }, []);

    const handleEnableScroll = useCallback(idScroll => {
      emitter.emmit(`${idScroll}-${ENABLE_SCROLL}`);
    }, []);

    return (
      <WrappedComponent
        {...props}
        onScrollToTop={handleScrollTop}
        onScrollTo={handleScrollTo}
        onScrollToBottom={handleScrollBottom}
        onScrollToChild={handleScrollToChild}
        onDisableScroll={handleDisableScroll}
        onEnableScroll={handleEnableScroll}
      />
    );
  };

  const getDisplayName = WrappedComponentIn =>
    WrappedComponentIn.displayName || WrappedComponentIn.name || 'Component';

  HOC.displayName = `CustomScrollHoc(${getDisplayName(WrappedComponent)})`;

  return memo(HOC);
};

export default CustomScrollHoc;
