import React from 'react';
import PropTypes from 'prop-types';
import debounce from 'lodash.debounce';
import throttle from 'lodash.throttle';
import emitter from './scrollEmitter';

import {
  CHANGE_SCROLL_POSITION,
  RESIZE_CONTAINER,
  END_SCROLL,
  ELEMENT_IN_VIEW,
} from './constants';

import { ScrollContext } from './scrollContext';

/* eslint-disable */

class OldElement extends React.PureComponent {
  static contextType = ScrollContext;
  constructor(props) {
    super(props);
    this.root = React.createRef();
    this.state = { inView: false };

    this.isResize = false;
    this.screenSpringFactor = 1;
    this.currentPosition = 0;
    this.scrollPosition = 0;

    // this.stateSkew = { y: 0 };

    this.defaultPosition = 0;
    this.defaultBottomPosition = 0;
    this.height = 0;

    this.visibility = true;

    this.stikyDist = 0;
    // this.maxStiky = 0;

    this.actionResizeContainerListener = `${props.idListener}-${RESIZE_CONTAINER}`;
    this.actionListener = `${props.idListener}-${CHANGE_SCROLL_POSITION}`;
    this.actionEndListener = `${props.idListener}-${END_SCROLL}`;
    this.actionInView = `${props.idListener}-${ELEMENT_IN_VIEW}`;

    this.handleDebounceResize = debounce(this.handleResize, 200);
    this.handleThrotleResize = throttle(this.handleTimeResize, 50);
    // this.handleThrotleChaneScroll = throttle(this.handleChangeScroll, 17);
  }

  componentDidMount() {
    const { hash } = this.props;
    if (hash) {
      this.context.addChild(this, hash);
    }
    this.innerHeight = window.__height;
    window.addEventListener('resize', this.handleDebounceResize);
    window.addEventListener('resize', this.handleThrotleResize);
    emitter.addListener(this.actionListener, this.handleChangeScroll);
    emitter.addListener(
      this.actionResizeContainerListener,
      this.handleResizeContainer
    );
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.handleDebounceResize);
    window.removeEventListener('resize', this.handleThrotleResize);
    emitter.removeListener(this.actionListener, this.handleChangeScroll);
    emitter.removeListener(
      this.actionResizeContainerListener,
      this.handleResizeContainer
    );
  }

  handleResizeContainer = () => {
    if (!this.root.current) return;

    const { y, height } = this.root.current.getBoundingClientRect();
    this.innerHeight = window.__height;
    this.defaultPosition = y;
    this.defaultBottomPosition = y + height;
    this.height = height;

    const screenPositionY = this.defaultPosition + this.currentPosition;
    const screenPositionBottom =
      this.defaultBottomPosition + this.currentPosition;

    this.outViewArea =
      screenPositionY > this.innerHeight / 2 ||
      screenPositionBottom < this.innerHeight / 2;

    if (this.outViewArea) {
      this.visibility = false;
      // this.setState({ inView: false });
    } else {
      this.visibility = true;
      emitter.emmit(this.actionInView, this.props.hash);

      if (!this.state.inView) {
        this.setState({ inView: true });
      }

      // this.props.emitter.emmit(this.actionInView, this.props.hash);
    }

    if (this.props.resizeCallback) {
      this.props.resizeCallback();
    }
  };

  handleTimeResize = () => {
    this.isResize = true;
    this.innerHeight = window.__height;
  };

  handleResize = () => {
    this.handleResizeContainer();
    this.isResize = false;
  };

  handleChangeScroll = (scrollPosition, maxScroll, isInfinity) => {
    const { springFactorFromWindow } = this.props;
    let springFactor = this.props.springFactor;
    if (this.isResize) springFactor = 1;

    const dScrollPosition =
      (scrollPosition - this.scrollPosition) * springFactor;
    this.currentPosition = this.isResize
      ? scrollPosition
      : this.currentPosition + dScrollPosition;
    this.scrollPosition = this.isResize
      ? scrollPosition
      : this.scrollPosition + dScrollPosition;

    const screenList = maxScroll + this.innerHeight;

    const screenPositionY = this.defaultPosition + this.currentPosition;
    const screenPositionBottom =
      this.defaultBottomPosition + this.currentPosition;

    if (isInfinity || springFactorFromWindow) {
      this.y = screenPositionY;
    }

    if (isInfinity) {
      const offset = this.height * 2;
      const bottomScreen = this.y - offset;
      const topScreen = this.y + offset;

      if (topScreen < 0) this.currentPosition += screenList;
      if (bottomScreen > this.innerHeight) this.currentPosition -= screenList;
    }

    if (!this.outViewArea && !this.visibility) {
      this.visibility = true;

      emitter.emmit(this.actionInView, this.props.hash);

      if (!this.state.inView) {
        requestAnimationFrame(() => {
          this.setState({ inView: true });
        });
      }

      // this.props.emitter.emmit(this.actionInView, this.props.hash);
    }

    if (this.outViewArea && this.visibility) {
      this.visibility = false;
      // this.setState({ inView: false });
    }

    this.outViewArea =
      screenPositionY > this.innerHeight * 0.75 ||
      screenPositionBottom < this.innerHeight * 0.75;
  };

  render() {
    const { children, className, tag } = this.props;
    const { inView } = this.state;
    const Tag = tag;

    return (
      <Tag className={className} ref={this.root}>
        {React.cloneElement(children, { inView })}
      </Tag>
    );
  }
}

OldElement.propTypes = {
  idListener: PropTypes.string.isRequired,
  children: PropTypes.any,
  className: PropTypes.string,
  springFactor: PropTypes.number,
  withSkew: PropTypes.bool,
  tag: PropTypes.string,
  hash: PropTypes.string,
  springFactorFromWindow: PropTypes.bool,
};

OldElement.defaultProps = {
  springFactorFromWindow: false,
  springFactor: 0.7,
  tag: 'div',
};

export default OldElement;
