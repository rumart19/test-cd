import React from 'react';
import { array, bool, object } from 'prop-types';
import { Switch, Route, useLocation } from 'react-router-dom';
import { TransitionGroup, Transition } from 'react-transition-group';

import isVisible from './utils/isVisible';
import { timeoutsShape } from './utils/propTypes';
import PageWrapper from 'components/PageWrapper';

import { gsap } from 'gsap';

const TransitionAppRouter = ({ routes, timeout }) => {
  const location = useLocation();

  const onEnterHandler = node => {
    gsap.killTweensOf(node);

    gsap.set(node, {
      autoAlpha: 0,
    });

    gsap.to(node, {
      autoAlpha: 1,
      delay: 0.7,
      duration: 0.8,
      ease: 'sine.in',
      onComplete: target => {
        gsap.set(target, {
          clearProps: 'position, width',
        });
      },
      onCompleteParams: [node],
    });
  };

  const onExitHandler = node => {
    gsap.killTweensOf(node);

    gsap.set(node, {
      position: 'fixed',
      left: 0,
      top: 0,
    });

    gsap.to(node, {
      duration: 0.6,
      autoAlpha: 0,
      ease: 'sine.out',
      filter: 'blur(60px)',
    });
  };

  return (
    <TransitionGroup component={null}>
      <Transition
        unmountOnExit
        timeout={timeout}
        key={location.pathname}
        onEnter={onEnterHandler}
        onExit={onExitHandler}
      >
        {status => (
          <Switch location={location} key={location.pathname}>
            {routes.map(
              ({ path, exact, title, component: Component, settings }) => (
                <Route
                  key={path}
                  exact={exact}
                  path={path}
                  render={props => {
                    return (
                      <PageWrapper
                        color={settings?.color}
                        title={title}
                        withContainer={settings?.withContainer}
                        isVisiblePage={isVisible(status)}
                        idScroll={settings?.idScroll}
                      >
                        <Component
                          idScroll={settings?.idScroll}
                          htmlContent={settings?.htmlContent}
                          transitionStatus={status}
                          {...props}
                        />
                      </PageWrapper>
                    );
                  }}
                />
              )
            )}
          </Switch>
        )}
      </Transition>
    </TransitionGroup>
  );
};

TransitionAppRouter.propTypes = {
  isOpenMenu: bool,
  location: object,
  routes: array,
  timeout: timeoutsShape,
};

export default TransitionAppRouter;
