import { number, shape, string, oneOfType } from 'prop-types';

export const timeoutsShape = oneOfType([
  number,
  shape({
    enter: number,
    exit: number,
  }).isRequired,
]);

export const classNamesShape = oneOfType([
  string,
  shape({
    enter: string,
    exit: string,
    active: string,
  }),
  shape({
    enter: string,
    enterDone: string,
    enterActive: string,
    exit: string,
    exitDone: string,
    exitActive: string,
  }),
]);
