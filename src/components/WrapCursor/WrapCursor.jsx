import React, { forwardRef, memo, useCallback, useMemo } from 'react';
import { any, string, object, bool, oneOfType, oneOf, func } from 'prop-types';
import { Link } from 'react-router-dom';
import classnames from 'classnames';

import modsClasses from 'utils/modsClasses';

import useCursorEvents from './useCursorEvents';

import s from './WrapCursor.scss';

const WrapCursor = forwardRef(
  (
    {
      as,
      to,
      href,
      children,
      disabled,
      className,
      hover,
      cbData,
      onClick,
      disableCursor,
      underline,
      colorUnderline,
      pointerDisable,
      pointerAll,
      preventClick,
      cursorScaleDown,
      cursorText,
      isHidden,
      onEnter,
      onLeave,
      ...otherProps
    },
    ref
  ) => {
    const {
      handleMouseLeave,
      handleMouseEnter,
      handleDoubleClick,
      handleMouseMove,
    } = useCursorEvents({
      ...otherProps,
      cursorScaleDown,
      disabled,
      disableCursor,
      cbData,
      cursorText,
      isHidden,
    });

    const CustomTag = useMemo(() => {
      if (as) return as;
      if (to) return Link;
      if (href) return 'a';
      return 'button';
    }, [to, href, as]);

    const classNames = useMemo(() => {
      const classes = modsClasses(s, {
        hover,
        colorUnderline,
      });

      return classnames(className, s.root, classes, {
        [s.disabled]: disabled,
        [s.underline]: underline,
        [s.pointerDisable]: pointerDisable,
        [s.pointerAll]: pointerAll,
      });
    }, [
      hover,
      className,
      disabled,
      underline,
      pointerDisable,
      colorUnderline,
      pointerAll,
    ]);

    const handleClick = useCallback(
      e => {
        if (preventClick) e.preventDefault();
        onClick(cbData, e);
      },
      [onClick, cbData, preventClick]
    );

    return (
      <CustomTag
        {...otherProps}
        to={to}
        href={href}
        ref={ref}
        rel={href && 'noopener noreferrer'}
        target={href && '_blank'}
        disabled={disabled}
        className={classNames}
        onClick={onClick && handleClick}
        onMouseLeave={e => {
          if (onLeave) onLeave();
          handleMouseLeave(e);
        }}
        onMouseEnter={e => {
          if (onEnter) onEnter();
          handleMouseEnter(e);
        }}
        onDoubleClick={handleDoubleClick}
        onMouseMove={handleMouseMove}
      >
        {children}
      </CustomTag>
    );
  }
);

WrapCursor.propTypes = {
  as: any,
  cbData: any,
  to: oneOfType([string, object]),
  href: string,
  children: any,
  disabled: bool,
  preventClick: bool,
  disableCursor: bool,
  underline: bool,
  pointerDisable: bool,
  cursorScaleDown: bool,
  pointerAll: bool,
  isHidden: bool,
  className: string,
  onClick: func,
  hover: oneOf(['testRed', 'opacity', 'underlineIn', 'underlineOut']),
  colorUnderline: oneOf(['black', 'variant', 'white']),
  cursorText: any,
  onEnter: func,
  onLeave: func,
};

WrapCursor.defaultProps = {
  disableCursor: false,
  isHidden: false,
  colorUnderline: 'variant',
};

export default memo(WrapCursor);
