import { useCallback, useEffect, useRef } from 'react';
import { useCursorEvents } from 'components/CustomCursor';
import useComponentDidMount from 'hooks/useComponentDidMount';

export default ({ disabled, disableCursor, cursorText, isHidden }) => {
  const refMount = useRef(false);
  const [onCursorEnter, onCursorLeave] = useCursorEvents();

  const handleMouseEnter = useCallback(() => {
    if (!disabled && !disableCursor) {
      onCursorEnter(cursorText, isHidden);
    }
  }, [onCursorEnter, disabled, disableCursor, cursorText, isHidden]);

  const handleMouseLeave = useCallback(() => {
    if (!disableCursor) onCursorLeave();
  }, [onCursorLeave, disabled, disableCursor]); // eslint-disable-line

  const handleMouseMove = useCallback(() => {
    if (!disabled && !disableCursor) onCursorEnter(cursorText, isHidden);
  }, [onCursorEnter, disabled, disableCursor, cursorText, isHidden]);

  useEffect(() => {
    if (!disabled && refMount.current) {
      onCursorLeave();
    }
  }, [disabled, onCursorLeave]);

  const handleDoubleClick = useCallback(e => {
    e.preventDefault();
  }, []);

  useComponentDidMount(() => {
    refMount.current = true;
  });

  return {
    handleMouseEnter,
    handleMouseLeave,
    handleMouseMove,
    handleDoubleClick,
  };
};
