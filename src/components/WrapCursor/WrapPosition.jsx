import { any, bool, func, object, oneOfType, string } from 'prop-types';
import { Link } from 'react-router-dom';
import React, { useEffect, useRef, useMemo, useCallback } from 'react';

import { useElement } from 'components/CustomScroll';

import useCursorEvents from './useCursorEvents';

const WrapPosition = ({
  as,
  to,
  href,
  children,
  className,
  cursorText,
  cursorScaleDown,
  disabled,
  disableCursor,
  cbData,
  onClick,
  onMouseEnter,
  onMouseLeave,
  onMouseMove,
  isHidden,
  isCheckScroll,
  ...otherProps
}) => {
  const rootRef = useRef(null);

  const {
    handleMouseLeave,
    handleMouseEnter,
    handleDoubleClick,
    handleMouseMove,
  } = useCursorEvents({
    ...otherProps,
    cursorScaleDown,
    disabled,
    disableCursor,
    cbData,
    cursorText,
    isHidden,
  });

  const [element] = useElement(rootRef, 'Home');
  const { cursorIn, yPos, offsetTop } = element;

  useEffect(() => {
    if (cursorIn) {
      handleMouseEnter();
    } else {
      handleMouseLeave();
      if (onMouseLeave) onMouseLeave();
    }
  }, [
    cursorIn,
    handleMouseEnter,
    handleMouseLeave,
    onMouseLeave,
    onMouseEnter,
  ]);

  const CustomTag = useMemo(() => {
    if (as) return as;
    if (to) return Link;
    if (href) return 'a';
    return 'button';
  }, [to, href, as]);

  const handleClick = useCallback(
    e => {
      if (onClick) onClick(cbData, e);
    },
    [onClick, cbData]
  );

  return (
    <CustomTag
      ref={rootRef}
      className={className}
      onMouseLeave={e => {
        handleMouseLeave(e);
        if (onMouseLeave) onMouseLeave();
      }}
      onMouseEnter={e => {
        handleMouseEnter(e);
        if (onMouseEnter) onMouseEnter(e, -yPos, offsetTop);
      }}
      onDoubleClick={handleDoubleClick}
      onMouseMove={e => {
        handleMouseMove(e);
        if (onMouseMove) onMouseMove(e, -yPos, offsetTop);
      }}
      onClick={handleClick}
    >
      {children}
    </CustomTag>
  );
};

WrapPosition.propTypes = {
  as: any,
  to: oneOfType([string, object]),
  href: string,
  children: any,
  className: string,
  cursorText: string,
  isHidden: bool,
  disabled: bool,
  isCheckScroll: bool,
  disableCursor: bool,
  cursorScaleDown: bool,
  onClick: func,
  onMouseEnter: func,
  onMouseLeave: func,
  onMouseMove: func,
  cbData: any,
};

export default WrapPosition;
