import React from 'react';

import Svg from 'components/Svg';

import { SOCIALS } from 'constants';
import { WrapCursor } from 'components/WrapCursor';

import s from './Socials.scss';

const Socials = () => {
  return (
    <ul className={s.root}>
      {SOCIALS.map(item => (
        <li className={s.item} key={item.name}>
          <WrapCursor as="a" className={s.link} href={item.href}>
            <Svg
              name={item.name}
              width="28px"
              height="28px"
              color="light"
              stroke="none"
            />
          </WrapCursor>
        </li>
      ))}
    </ul>
  );
};

export default Socials;
