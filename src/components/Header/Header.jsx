import React, { memo, useCallback, useRef, useMemo } from 'react';
import { func } from 'prop-types';

import classes from 'classnames';

import { CustomScrollHoc } from 'components/CustomScroll';

import Socials from './Socials';
import Menu from './Menu';
import Svg from 'components/Svg';
import { WrapCursor } from 'components/WrapCursor';

import useSelector from 'hooks/useSelector';
import {
  isHeaderBlendSelector,
  isOpenMenuSelector,
} from 'models/common/selectors';

import useAction from 'hooks/useAction';
import { actions } from 'models/common/slice';

import { useLocation, useHistory } from 'react-router-dom';

import useComponentDidMount from 'hooks/useComponentDidMount';

import s from './Header.scss';

const Header = ({ onScrollToTop }) => {
  const location = useLocation();
  const history = useHistory();

  const isHomePage = useMemo(() => location.pathname === '/', [location]);

  const isBlend = useSelector(isHeaderBlendSelector);

  const refHeader = useRef(null);

  const setHeight = useAction(actions.setHeaderHeight);

  const isOpenMenu = useSelector(isOpenMenuSelector);
  const setIsOpenMenu = useAction(actions.setIsOpenMenu);

  const handleClickLogo = useCallback(() => {
    if (isHomePage && !isOpenMenu) {
      onScrollToTop('Home', 2);
    } else if (isOpenMenu) {
      setIsOpenMenu(false);
    } else {
      history.push('/');
    }
  }, [onScrollToTop, isHomePage, history, isOpenMenu, setIsOpenMenu]);

  const handleResize = useCallback(() => {
    setHeight(refHeader.current.clientHeight);
  }, [setHeight]);

  useComponentDidMount(() => {
    setTimeout(() => {
      handleResize();
    }, 300);

    window.addEventListener('resize', handleResize);

    return () => {
      window.removeEventListener('resize', handleResize);
    };
  });

  const handleOpenMenu = () => {
    setIsOpenMenu(!isOpenMenu);
  };

  return (
    <header
      ref={refHeader}
      className={classes(s.root, {
        [s.isBlend]: (isBlend && isHomePage) || isOpenMenu,
        [s.isNotHome]: !isHomePage,
      })}
    >
      <div className={s.inner}>
        <Socials />
        <WrapCursor className={s.logo} onClick={handleClickLogo} as="div">
          <Svg name="logo" width="45px" height="45px" />
        </WrapCursor>
        <Menu onOpenMenu={handleOpenMenu} isOpenMenu={isOpenMenu} />
      </div>
    </header>
  );
};

Header.propTypes = {
  onScrollToTop: func,
};

export default CustomScrollHoc(memo(Header));
