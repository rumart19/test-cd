import React, { useRef } from 'react';

import AudioButton from 'components/AudioButton';

import classes from 'classnames';

import { WrapCursor } from 'components/WrapCursor';

import { useLocation } from 'react-router-dom';

import s from './Menu.scss';
import { bool, func } from 'prop-types';

const Menu = ({ onOpenMenu, isOpenMenu }) => {
  const location = useLocation();

  const menuTextRef = useRef(null);

  const isContentPage = () => {
    return location.pathname === '/content';
  };

  const handleOpenMenu = () => {
    if (onOpenMenu) onOpenMenu();
  };

  return (
    <ul className={s.root}>
      <li className={s.item}>
        <AudioButton />
      </li>
      <li className={s.item}>
        <WrapCursor
          as="button"
          className={s.btn}
          type="button"
          onClick={handleOpenMenu}
        >
          <span ref={menuTextRef}>{isOpenMenu ? 'Close' : 'Menu'}</span>
        </WrapCursor>
      </li>
      <li className={s.item}>
        <WrapCursor
          to="/content"
          className={classes(s.more, {
            [s.moreActive]: isContentPage(),
          })}
          type="button"
        >
          <div className={s.dot} />
          <div className={s.dot} />
        </WrapCursor>
      </li>
    </ul>
  );
};

Menu.propTypes = {
  onOpenMenu: func,
  isOpenMenu: bool,
};

export default Menu;
