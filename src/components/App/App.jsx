import React, { useEffect } from 'react';
import { array } from 'prop-types';
/* import { Helmet } from 'react-helmet'; */

import { TransitionAppRouter } from 'components/AppRouter';

import 'styles/normalize.scss';
import 'styles/fonts.scss';
import 'styles/global.scss';
import 'styles/common.scss';

import { CSSTransition } from 'react-transition-group';

import { PAGE_TRANSITION } from 'constants';

import { useLocation } from 'react-router-dom';

import useViewport from 'hooks/useViewport';

import Noise from 'components/Noise';
import Header from 'components/Header';
import Menu from 'components/Menu';
import MainVideo from 'components/MainVideo';
import Helper from './Helper';
// import PixiApp from 'components/PixiApp';

import useSelector from 'hooks/useSelector';

import {
  isMainVideoShowSelector,
  styleColorSelector,
  isOpenMenuSelector,
} from 'models/common/selectors';

import useAction from 'hooks/useAction';
import { actions as uiActions } from 'models/common/slice';

import { LoaderContainer } from 'components/Loader';

import CustomCursor from 'components/CustomCursor';

import s from './App.scss';

const App = ({ routes }) => {
  const { pathname } = useLocation();
  const isMainVideoShow = useSelector(isMainVideoShowSelector);
  const isOpenMenu = useSelector(isOpenMenuSelector);

  const setIsOpenMenu = useAction(uiActions.setIsOpenMenu);

  const setIsMainVideoShow = useAction(uiActions.setShowMainVideo);
  const styleColor = useSelector(styleColorSelector);

  const handleVideoClose = () => {
    setIsMainVideoShow(false);
  };

  useEffect(() => {
    setIsOpenMenu(false);
  }, [pathname, setIsOpenMenu]);

  const { isDeviceDesktop } = useViewport();

  return (
    <div
      className={s.root}
      style={{
        '--styleColor': styleColor,
      }}
    >
      <Helper />
      {/* <PixiApp /> */}
      <CSSTransition
        in={isMainVideoShow}
        timeout={400}
        unmountOnExit
        classNames="fade"
      >
        <MainVideo onClose={handleVideoClose} />
      </CSSTransition>
      <CSSTransition
        in={isOpenMenu}
        timeout={400}
        unmountOnExit
        classNames="fade"
      >
        <Menu />
      </CSSTransition>
      <Noise />
      <Header />
      <LoaderContainer>
        {isDeviceDesktop && <CustomCursor />}
        <TransitionAppRouter timeout={PAGE_TRANSITION} routes={routes} />
      </LoaderContainer>
    </div>
  );
};

App.propTypes = {
  routes: array.isRequired,
};

export default App;
