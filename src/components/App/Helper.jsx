import React, { useRef, memo } from 'react';
import useMediaHelper from 'hooks/useMediaHelper';

const Helper = () => {
  const ref = useRef();
  useMediaHelper(ref);

  return (
    <div
      ref={ref}
      style={{
        position: 'fixed',
        top: '0',
        bottom: '0',
      }}
    />
  );
};

export default memo(Helper);
