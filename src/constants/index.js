export const DESKTOP = 'DESKTOP';
export const TABLET = 'TABLET';
export const MOBILE = 'MOBILE';

export const PAGE_TRANSITION = 900;

export const COLORS = {
  dark: 'dark',
  white: 'white',
};

export const STYLE_COLOR = '#f9f8e4';

export const MOBILE_MEDIA_QUERY = '(max-width: 768px)';
export const TABLET_MEDIA_QUERY = '(max-width: 768px) and (min-width: 481px)';

export const SOCIALS = [
  {
    href: '/',
    name: 'yt',
  },
  {
    href: '/',
    name: 'inst',
  },
  {
    href: '/',
    name: 'tiktok',
  },
  {
    href: '/',
    name: 'ghost',
  },
  {
    href: '/',
    name: 'tweet',
  },
  {
    href: '/',
    name: 'facebook',
  },
];

export const MAIN_MENU = [
  {
    id: 1,
    title: 'Home',
    to: '/',
  },
  {
    id: 2,
    title: 'Store',
    to: '/',
  },
  {
    id: 3,
    title: 'Content',
    to: '/content',
  },
  {
    id: 4,
    title: 'About Us',
    to: '/',
  },
];
