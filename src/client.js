import React from 'react';
import { render, unmountComponentAtNode } from 'react-dom';
import { Provider } from 'react-redux';
import { createBrowserHistory } from 'history';
import { ConnectedRouter } from 'connected-react-router';

import routes from './routes';
import { rootSaga } from 'models';
import configureStore from './store';

import './components/App';

import { actions } from 'models/common/slice';
import { onCheckViewport } from 'hooks/useMediaHelper/utils';

/* Get initial state from server side rendering */
const initialState = window.__INITIAL_STATE__;

const history = createBrowserHistory();
const store = configureStore(history, initialState);

/* Start saga middleware */
store.runSaga(rootSaga);

store.dispatch({
  type: actions.switchViewport,
  payload: onCheckViewport(),
});

store.dispatch({
  type: actions.changeScreenSize,
  payload: {
    width: window.innerWidth,
    height: window.__height,
  },
});

const mountNode = document.getElementById('react-view');

const renderApp = () => {
  if (process.env.APP_ENV === 'development') {
    unmountComponentAtNode(mountNode);
  }
  const App = require('./components/App').default;

  render(
    <Provider store={store}>
      <ConnectedRouter history={history}>
        <App routes={routes} />
      </ConnectedRouter>
    </Provider>,
    mountNode
  );
};

if (module.hot) {
  module.hot.accept();
}

document.addEventListener('DOMContentLoaded', () => {
  requestAnimationFrame(renderApp);
});
