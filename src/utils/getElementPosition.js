import { getMousePos } from 'components/CustomCursor/utils';

export default (event, image, y, offsetTop) => {
  const mousePos = getMousePos(event);
  const realYPos = mousePos.y + y;

  const mX = mousePos.x - image.clientWidth / 2;
  const mY = realYPos - offsetTop - image.clientHeight / 2;

  return { x: mX, y: mY };
};
