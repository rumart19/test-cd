export const mix = (x, y, a) => {
  return x * (1 - a) + y * a;
};

export const clamp = (x, minVal, maxVal) => {
  return Math.min(Math.max(x, minVal), maxVal);
};

export const outClamp = (x, minVal, maxVal) => {
  return Math.max(Math.min(x, minVal), maxVal);
};

export const round = (t, i) => {
  i = 0 !== i ? Math.pow(10, i) : 1e3; // eslint-disable-line
  return Math.round(t * i) / i;
};

export const vhToPx = vh => {
  return (vh / 100) * window.__height;
};

export const getUvRate = (width, height) => {
  if (width > height) {
    return { x: 1, y: width / height };
  }

  return { x: height / width, y: 1 };
};

export const randomInRange = (min, max) => {
  return Math.floor(Math.random() * (max - min + 1)) + min;
};

export const getRandomInt = max => {
  return Math.floor(Math.random() * Math.floor(max));
};

export const fract = x => {
  return x - Math.floor(x);
};

export const getFirstDigit = position => {
  return Number(position.toFixed(0)[0]);
};

export const calculateCircumference = radius => {
  return 2 * Math.PI * radius;
};

export const windowRate = ({ width, height }) => {
  if (width > height) {
    return { x: 1, y: width / height };
  }

  return { x: height / width, y: 1 };
};

export const getRadiusCircle = ({ width, height }) => {
  const diagonal = Math.hypot(width, height);
  const normalizeD = diagonal / Math.min(width, height);
  return normalizeD / 2;
};
