import gsap from 'gsap';

const addEvents = (el, eventHandlers) => {
  eventHandlers.forEach(([e, h, o]) => el.addEventListener(e, h, o));
};

const removeEvents = (el, eventHandlers) => {
  eventHandlers.forEach(([e, h, o]) => el.removeEventListener(e, h, o));
};

const draggble = (node, refs, options) => {
  let scrollY = 0;
  let y = 0;

  let touchStart = 0;
  let touchX = 0;
  let isDragging = false;
  let isEnter = false;

  let itemWidth = 0;
  let distantion = 0;

  const initAnimation = () => {
    const elements = refs.current;
    itemWidth = gsap.getProperty(elements[0], 'width');
    distantion = itemWidth * elements.length;
  };

  const dispose = scroll => {
    gsap.set(refs.current, {
      x: i => {
        return i * itemWidth + scroll;
      },
      modifiers: {
        x: gsap.utils.unitize(x => {
          return gsap.utils.wrap(
            -itemWidth,
            distantion - itemWidth,
            parseFloat(x)
          );
        }),
      },
    });
  };

  const render = () => {
    if (!isEnter && options.autoPlay) scrollY += 3;
    y = y * (1 - 0.1) + scrollY * 0.1;
    dispose(y);
  };

  const handleTouchStart = e => {
    if (isDragging) return;
    touchStart = e.clientX || e.touches[0].clientX;
    isDragging = true;
  };

  const handleTouchMove = e => {
    if (!isDragging) return;
    touchX = e.clientX || e.touches[0].clientX;
    scrollY += (touchX - touchStart) * 2.5;
    touchStart = touchX;
  };

  const handleTouchEnd = e => {
    isDragging = false;
    if (e.type && e.type === 'mouseleave') isEnter = false;
  };

  const handleMouseWheel = e => {
    scrollY -= e.deltaX * 0.3;
  };

  const handleMouseEnter = () => {
    isEnter = true;
  };

  const moveHandlers = [
    ['mousedown', handleTouchStart],
    ['mousemove', handleTouchMove],
    ['mouseleave', handleTouchEnd],
    ['mouseup', handleTouchEnd],
    ['mousewheel', handleMouseWheel],
    ['mouseenter', handleMouseEnter],
    ['touchstart', handleTouchStart],
    ['touchend', handleTouchEnd],
    ['touchmove', handleTouchMove, { passive: false }],
    ['selectstart', () => false],
  ];

  const windowHandlers = [['resize', initAnimation]];

  return {
    init: () => {
      initAnimation();
      addEvents(node, moveHandlers);
      addEvents(window, windowHandlers);
      gsap.ticker.add(render);
    },
    destroy: () => {
      removeEvents(node, moveHandlers);
      removeEvents(window, windowHandlers);
      gsap.ticker.remove(render);
    },
    enter: () => {
      isEnter = true;
    },
  };
};

export default draggble;
